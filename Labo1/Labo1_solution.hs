multid x y = x*y

somme x = sum [1..x]

somme1 1 = 1 
somme1 n = somme1 (n - 1) + n

somme2 n = if n == 1 then 1 else somme2 (n - 1) + n 

f x y = 7*(x^3) + 2*(y^2)

racine x = if x<0 then error "argument negatif" else sqrt x

maxi x y | x<y = y
         | otherwise = x

impaire x = if (odd x) then True else False 
impaire1 a = a `rem` 2 /= 0 
impaire2 a = a `mod` 2 == 1

pgdc 0 b = b
pgdc a b = pgdc (b `mod` a) a -- algo d'euclide

listeInverse [] = []
listeInverse (x:xs) = listeInverse xs ++ [x]

listeTriee [] = []
listeTriee (x:xs) = listeTriee x1 ++ [x] ++ listeTriee x2 
                    where 
                        x1 = [y| y <- xs, y<=x] 
                        x2 = [y| y <- xs, y>x]

compteurInt _ [] = 0
compteurInt y (x:xs) | x==y = 1 + compteurInt y xs
                     | otherwise = compteurInt y xs