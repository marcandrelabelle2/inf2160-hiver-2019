## 1 - Premiers pas vers haskell 
Pour lancez l'interpréteur de GHC, il suffit d'entrer la commande :
> ghci

**Remarque** : Il existe un grand nombre de commandes dans _GHC_, nous n'allons voir que celles qui sont pertinentes pour cette démo. Pour les curieux, vous trouverez sur ce [site](https://downloads.haskell.org/~ghc/7.4.1/docs/html/users_guide/ghci-commands.html) une liste non exhaustive des autres commandes .  
### 1.1 - Test de fonctions
Haskell possède des fonctions intégrées. Cette partie consiste en l’évaluation d’un certain nombre de ces fonctions. Pour chacune des expressions ci-dessous, donner la réponse et une description de ce que fait la fonction.
```haskell
1. log 5.2
2. max 3 2
3. min 3 2
4. maximum [20, 0 , -5, 22, 13]
5. abs (-2)
6. not (4 == 3)
7. map sqrt [1..5]
8. map (^2) [1..9]
9. 16 `mod` 9
10. 16 `div` 9
11. ceiling 7.8
12. compare "automne" "zizou"
13. odd (2 * 10)
14. floor 2.7
15. atan pi
16. cos pi
17. head [1..20]
18. head ["ceci", "et", "cela"]
19. init [1..20]
20. last [1..20]
21. take 10 [2,4..]
22. length [1..10]
23. pred 1
24. exp 1
25. fst (2, 3)
26. gcd 2 10
27. lcm 2 10
```

### 1.2 - Écriture de fonctions simples
Notez qu'il suffit d'appuyer sur _CTRL-D_ pour quitter l'interpréteur. Dans cette section et dans celles à venir, vous devez écrire vos solutions dans un fichier de script ayant pour extension `.hs`.
Pour charger votre programme `script.hs` dans ghci, vous devez saisir l'instruction :
```haskell
:l script.hs
```
Lorsque le chargement est complété, vous pouvez sans problème utiliser les
fonctions définies dans votre fichier. Il est aussi possible de compiler le module _GHC_ pour qu'il produise un exécutable. Pour cela, il suffit d'entrer la commande :
```haskell
ghc -o sc script.hs
```
qui produit plusieurs fichiers `(script.o, script.hi, sc)`. Évidemment, vous pouvez remplacer _sc_ par n'importe quel autre nom de votre choix. Puis, vous pouvez exécuter le programme avec :
```haskell
./sc
```
Pendant la première partie de la session, vous allez principalement développer
vos programmes Haskell à l'aide de l'interpréteur. Plus précisément, vous allez
écrire des fonctions dans un fichier avec l'extension `.hs` que vous allez
charger dans l'interpréteur afin d'effectuer différents tests. La compilation
en un fichier exécutable sera plus rarement utilisée.

#### 1.2.1 - multid :
Définissez la fonction `multid` qui renvoie le produit de deux valeurs. Par exemple:
```haskell
Main> multid 2 3
6
```
#### 1.2.2 - somme :
Définissez la fonction `somme` qui permet de calculer, _somme(n) = 1 + 2 + ... + n_ où `n` est un entier.
```haskell
Main> somme 2
3
Main> somme 5
15
```
#### 1.2.3 - f :
Définissez la fonction `f(x, y)` qui permet de calculer _f(x, y) = 7 x<sup>3</sup> + 2 y<sup>2</sup>_
```haskell
Main> f 1 1
9
Main> f 2 2
64
```

## 2 - Structures de contrôle
### 2.1 - Racine :
Ecrire la fonction `racine :: Float -> Float` qui prend un argument non négatif en paramètre. Si cet argument est négatif, le programme doit s’arrêter et imprimer le message `"Erreur : argument negatif"`. Sinon, la fonction retourne la racine carrée (_sqrt_ prédéfinie en Haskell) de son paramètre.
```haskell
Main> racine (-7)
Program error: argument negatif
Main> racine 8
2.82842712474619
```

### 2.2 - Maxi :
Définissez la fonction `maxi` (car la fonction max est prédéfinie) qui renvoie la plus grande de deux valeurs. Par exemple:
```haskell
Main> maxi 5 2
5
```

### 2.3 - Impaire :
Définissez la fonction `impaire` qui vérifie si un nombre est impair. Par exemple:
```haskell
Main> impaire 8
False
Main> impaire 7
True
```

### 2.4 - Pgdc :
Définissez la fonction `pgdc :: Integer -> Integer -> Integer` qui trouve le plus grand diviseur commun entre deux nombres.
```haskell
Main> pgdc 36 63
9
Main> pgdc 5 9
1
```

## 3 - Manipulation de listes (1ere Partie}
### 3.1 -listeInverse :
Définissez la fonction `listeInverse` qui prend une liste et renvoie la même liste dans l'ordre inverse (ne pas utiliser la fonction `reverse`). Par exemple:
```haskell
Main> listeInverse [1,2,3,4,5]
[5,4,3,2,1]
Main> listeInverse ['o', 'u', 'i']
"iuo"
```

### 3.2 -listeTriee :
Définissez la fonction `listeTriee` qui prend une liste et renvoie la même liste ordonnée (ne pas utiliser la fonction `sort`). Par exemple:
```haskell
Main> listeTriee [1,43,5,4,6543,647,5,4325,3654,4,45,5,65,3,23,43,654]
[1,3,4,4,5,5,5,23,43,43,45,65,647,654,3654,4325,6543]
Main> listeTriee "Bonjour, comment ça va ?"
" ,?Baacejmmnnooortuv\129\231"
Main> listeInverse (listeTriee "QWERTY")
"YWTRQE"
```

### 3.3 -compteurInt :
Définissez la fonction `compteurInt` qui prend un entier une liste d'entiers et renvoie le nombre d'occurences de ce nombre. Par exemple:
```haskell
Main> compteurInt 3 [1,2,3,4,5,4,3,2,1]
2
Main> compteurInt 8 [1,2,3,4,5,4,3,2,1]
0
```

