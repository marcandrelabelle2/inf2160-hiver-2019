## Fonctions d'ordre supérieures


### 1- foldr, foldr1, foldl, fodl1
Que retourne chacune de ces expressions ? 
```haskell
foldr1 (-) [1,2,3,4] 
foldr (-) 4 [1,2,3] 
foldl1 (-) [4,1,2,3] 
foldl (-) 4 [1,2,3] 
```

### 2- Somme et produit
À l'aide des fonctions `foldr` et ou `foldl`, définissez les fonctions `somme` et `produit` qui
donnent respectivement la somme et le produit d'une liste de nombres. Par exemple:
```haskell
Main> somme [pi, 5.25, -3] 
5.39159
Main> produit [pi, 1/2, 10] 
15.708
Main> somme []
0
Main> :type produit
produit :: Num a => [a] -> a
```

### 3- map’ et filter’
Redéfinir `map` et `filter` en utilisant la fonction `foldr`. Par exemple :
```haskell
Main> map' (+3) 
[1,2,3,4,5] [4,5,6,7,8]
Main> filter' (> 2) [3,4,2,1,5] 
[3,4,5]
```

### 4- listeImaginaire
Exprimez cette liste de compréhension 
```haskell
[f x | x <- xs, p x] 
```
en utilisant les fonctions `map` et `filter`.
```haskell
Main> listeImaginaire (*2) (>10) [2,3,20,15] 
[40,30]
Main> listeImaginaire (*2) (>10) [2,3]
[]
```

### 5- any1
Redéfinissez la fonction `any` en utilisant la fonction `foldr`. Par exemple :
```haskell
Main> any1 (>3) [1,2,3,4,5]
True
Main> :t any1
any1 :: Foldable t => (a -> Bool) -> t a -> Bool
```

### 6- any2
Redéfinissez la fonction `any` mais cette fois ci en utilisant `foldl`. Par exemple:
```haskell
Main > any2 (>3) [1,2,3,4,5]
True
Main> :t any2
Any2 :: Foldable t => (a -> Bool) -> t a -> Bool
```
### 7- triInsertion
Soit la fonction `inserer` suivante:
```haskell
inserer e [] = [e] 
inserer e (x:xs)
  | e > x = x: inserer e xs
  | otherwise = e:x:xs
```
Le tri insertion consiste à insérer le premier élément dans une liste vide, puis le deuxième au bon endroit dans cette liste, et ainsi de suite. Définissez la fonction `triInsertion` en utilisant un `foldr` ou `foldl` et la fonction `inserer` ci–dessus. Par exemple:
```haskell
Main> triInsertion [54,76,34,99,12,76,23,64,82,26]
[12,23,26,34,54,64,76,76,82,99]
Main> triInsertion [[7,6,5], [1,2,3], [5,9,2], [], [8,8], [9]]
[[],[1,2,3],[5,9,2],[7,6,5],[8,8],[9]]
Main> triInsertion "Bonjour, comment ca va?" 
" ,?Baaccejmmnnooortuv"
Main> :type triInsertion
triInsertion :: Ord a => [a] -> [a]
```

### 8- norme
Définissez à l’aide de la fonction `map` et d’une `lamda expression`, la fonction `norme` qui prend en paramètre une liste et retourne la racine carrée de la somme des carrés de tous les éléments de la liste. Vous devez utiliser au moins une fois la fonction `$`. Par exemple
```haskell
Main > norme [2,3] 
3.605551275463989 
Main > norme [] 
0.0
```

### 9- positionElts
Définissez la fonction `positionElts` qui prend une liste et renvoie une liste de couples (élément, position). Vous devez utiliser la fonction prédéfinie `zip`. Par exemple:
```haskell
Main> positElts [9,8,7,8,9] 
[(9,1),(8,2),(7,3),(8,4),(9,5)]
Main> positionElts [True, False, True] 
[(True,1),(False,2),(True,3)]
Main> triInsertion (positionElts "Bonjour")
[('B',1),('j',4),('n',3),('o',2),('o',5),('r',7),('u',6)]
```

### 10- listeDoubles
Définissez la fonction `listeDoubles` qui prend une liste de nombres en paramètre et renvoie la liste de ces nombres doublés. Vous devez utiliser la fonction prédéfinie `map` dans votre définition. Par exemple:
```haskell
Main> listeDoubles [1,2,3] 
[2,4,6]
Main> listeDoubles [-7, -4 .. 7] 
[-14,-8,-2,4,10]
```
_Remarque_ : Faites une définition qui fonctionne pour tous les types de nombres. Par exemple:
```haskell
Main> listeDoubles [pi, 1.3, 3.25]
[6.28319,2.6,6.5]
```

### 11- indiquerPseudoPremiers
La fonction suivante renvoie `True` pour les nombres premiers et `False` pour la grande majorité des autres nombres. Elle permet de déterminer rapidement si un nombre a des chances d'être premier. On dit que ces nombres sont `pseudo-premiers`.
```haskell
pseudoPremier x = 2^(x-1) `mod` x == 1 
```
Par exemple:
```haskell
Main> pseudoPremier 4 
False
Main> pseudoPremier 41 
True
Main> pseudoPremier 341
True -- 341 = 11*31
```
Faites une fonction `indiquerPseudoPremiers` qui prend une liste d'entiers et renvoie une liste de couples formés de chacun des entiers et d'un booléen qui indique si le nombre est pseudo-premier. Par exemple:
```haskell
Main> indiquerPseudoPremiers [4, 41, 341]
[(4,False),(41,True),(341,True)]
Main> indiquerPseudoPremiers [101, 27, 25, 23, 21, 19, 557, 559]
[(101,True),(27,False),(25,False),(23,True),(21,False),(19,True),(557,True),(559, False)]
Main> :type indiquerPseudoPremiers
indiquerPseudoPremiers :: Integral a => [a] -> [(a,Bool)]
```

### 12- sommeCarresPairs
Définissez la fonction `sommeCarresPairs` qui prend une liste et renvoie la somme des carrés des nombres pairs de la liste. Vous devez utiliser l’opérateur composition de fonctions `(.)`, la fonction `filter` et la fonction `map`. Par exemple :
```haskell
Main > sommeCarresPairs [1,2,5,3,7] 
4
Main > sommeCarresPairs []
0
```

### 13- puiss2n et puiss2mn
`scanl1` et `scanr1` sont comme `foldl1` et `foldr1`, mais rapportent tous les résultats intermédiaires de l’accumulateur sous forme d’une liste. Par exemple :
```haskell
Main> scanl (+) 0 [1,2,3] 
[0,1,3,6]
Main> scanl1 (+) [1,2,3] 
[1,3,6]
```
À l'aide des fonctions `scanl1, take, drop et repeat`, définissez la fonction `puiss2n` qui donne les n premières puissances de deux (2, 4, 8, 16, etc.), puis la fonction `puiss2mn` qui donne les puissances de deux de la m-ième à la n-ième. Par exemple:
```haskell
Main> puiss2n 0
[]
Main> puiss2n 15 
[2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768] 
Main> :type puiss2n
puiss2n :: Int -> [Int]
Main> puiss2mn 3 3
[8]
Main> puiss2mn 10 15 
[1024,2048,4096,8192,16384,32768] 
Main> puiss2mn 10 1
[]
Main> :type puiss2mn 
puiss2mn :: Int -> Int -> [Int]
```
