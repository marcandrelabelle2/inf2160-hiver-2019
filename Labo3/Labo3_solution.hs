somme xs = foldr (+) 0 xs
somme' xs = foldl (+) 0 xs

produit xs = foldr (*) 1 xs
produit' xs = foldl (*) 1 xs

map' f = foldr (\x acc -> (f x):acc) []
map'' f xs = foldl (\acc x -> acc ++ [(f x)]) [] xs

filter' f xs = foldr(\x acc -> if(f x) then x:acc else acc) [] xs
filter'' f  = foldl(\acc x -> if(f x) then acc++[x] else acc) []

listeImaginaire f p xs = map f (filter p xs)

any1 f xs = foldr (\x acc -> f x || acc) False xs
any2 f xs = foldl (\acc x -> f x || acc) False xss

inserer e [] = [e]
inserer e (x:xs)
     | e > x = x: inserer e xs
     | otherwise = e:x:xs
triInsertion xs = foldr inserer [] xs
triInsertion'' xs = foldl (flip inserer) [] xs

inserer' [] e = [e]
inserer'(x:xs) e
     | e > x = x: inserer' xs e
     | otherwise = e:x:xs
triInsertion':: Ord a => [a] -> [a]
triInsertion' = foldl inserer' [] 


normeSans xs = sqrt (sum (map (\x-> x^2) xs))
norme xs = sqrt $ sum $ map (^2) xs
norme' xs = sqrt $ sum $ map (\x-> x^2) xss

positionElts xs = zip xs [1..length xs]

listeDoubles xs = map (*2) xs

pseudoPremier x = 2^(x-1) `mod` x == 1
indiquerPseudoPremiers xs = zip xs (map pseudoPremier xs)

sommeCarresPairs   = sum.map (^2). filter even 

puiss2n n = scanl1 (*) $ take n $ repeat 2
puiss2mn m n = drop (m-1) (puiss2n n)