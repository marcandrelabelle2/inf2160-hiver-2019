{-  
1) sort nombre
2) nub nombre
3) maximum nombres - minimum nombres
4) realToFrac((sum nombres)) / genericLength nombres)
5) take 10 (filter odd nombres)
6) (div 147457 1297, mod 147457 1297)
7) sum [1..100] == (100*101/2)
8) length (permutations [1..8]) == factorial 8
9) tanspose matrice == matrice
10) map toLower phrase
11) filter isLower (map toLower phrase)
12) reverse (filter isLower (map toLower phrase)) == (filter isLower (map toLower phrase))
13) length (filter isSpace phrase) 
14) length (filter isUpper phrase)
15) length code == 12 && all isAlpha (take 4 code) && all isDigit (drop 4 code)
 -}
 
compteur:: Eq a=> a->[a]->Int
compteur _ [] = 0
compteur y (x:xs) | x==y = 1 + compteur y xs
                  | otherwise = compteur y xs

compteurFiltre :: (a->Bool)->[a]->Int 
compteurFiltre _ [] = 0                 
compteurFiltre f (x:xs) | f x = 1+ compteurFiltre f xs
                        | otherwise = compteurFiltre f xs
                   
compteurFiltre' :: (a->Bool)->[a]->Int
compteurFiltre' f xs = length [x|x<-xs, f x]

elem' :: Eq a =>a -> [a]->Bool
elem' _ [] = False
elem' y (x:xs) = if(x==y) then True else elem' y xs

elem'':: Eq a=> a->[a]->Bool
elem'' y xs = length [x|x<-xs,x==y] > 0

ensemble :: Eq a => [a] -> [a]
ensemble [] = []
ensemble (x:xs) = if(elem' x xs) then ensemble xs else (x:ensemble xs)

ensemble' :: Eq a => [a] -> [a]
ensemble' xs = [x|(y,x)<-zip [1..length xs] xs, not (elem' x (drop y xs))]

intersection :: Eq a => [a] -> [a] -> [a]
intersection xs ys = [ x | x <- xs, elem x ys]

intersection' :: Eq a => [a] -> [a] -> [a]
intersection' [] _ = []
intersection' (x:xs) ys | elem x ys = x:intersection' xs ys
                        | otherwise = intersection' xs ys

ensemblesEgaux :: Eq a => [a] -> [a] -> Bool
ensemblesEgaux xs ys = (length (intersection xs ys) == length xs) && (length(intersection xs ys) == length ys)

supprimerPremier:: Eq a => a->[a]->[a]
supprimerPremier _ [] = []
supprimerPremier y (x:xs)= if(y==x) then xs else (x:supprimerPremier y xs)

