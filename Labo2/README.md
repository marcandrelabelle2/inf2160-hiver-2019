## 1 - Signature et règles de correspondance

Rendez-vous sur la page de [documentation officielle du module
`Data.List`](https://hackage.haskell.org/package/base-4.10.1.0/docs/Data-List.html)
de Haskell.

Portez attention en particulier aux signatures et aux règles de correspondance
des fonctions qui sont fournies par le module. Ignorez pour le moment, les mots `Maybe`, `Foldable`,
etc., ceux-si seront expliqués dans les labos ultérieurs.

Par contre, voici quelques explications sommaires sur les expressions `Num a
=>`, `Ord a =>` et `Eq a =>` :

- L'expression `Num a =>` indique que le type `a` doit supporter les opérations
  habituelles auxquelles on s'attend pour un type *numérique*, par exemple `+`,
  `-`, `*`, etc.
- L'expression `Ord a =>` indique que les éléments de type `a` doivent être
  *comparables*, c'est-à-dire qu'on peut toujours évaluer les expressions `x <
  y`, `x == y` et `x > y`.
- Finalement, l'expression `Eq a =>` indique que les éléments de type `a`
  doivent être *égalisables*, c'est-à-dire qu'on peut évaluer `x == y`.

Familiarisez-vous plus précisément avec les fonctions suivantes (notez que certaines signatures ont été simplifiées) :
```haskell
-- Fonctions de base
(++) :: [a] -> [a] -> [a]                -- Concaténation de deux listes
head :: [a] -> a                         -- Premier élément d'une liste
last :: [a] -> a                         -- Dernier élément d'une liste
tail :: [a] -> [a]                       -- Queue d'une liste
init :: [a] -> [a]                       -- Partie initiale d'une liste
null :: [a] -> Bool                      -- Indique si une liste est vide
length :: [a] -> Int                     -- Longueur d'une liste
replicate :: Int -> a -> [a]             -- Répète un élément plusieurs fois dans une liste
(!!) :: [a] -> Int -> a                  -- Retourne le i-ème élément d'une liste

-- Sous-listes
take :: Int -> [a] -> [a]                -- Prend le préfixe d'une liste de longueur donnée
drop :: Int -> [a] -> [a]                -- Prend le suffixe d'une liste en supprimant le nombre
                                         -- donné d'éléments au début

-- Transformations
map :: (a -> b) -> [a] -> [b]            -- Applique une fonction à chaque élément d'une liste
filter :: (a -> Bool) -> [a] -> [a]      -- Ne conserve que les éléments d'une liste vérifiant une condition
reverse :: [a] -> [a]                    -- Renverse une liste
intersperse :: a -> [a] -> [a]           -- Insère des copies d'un élément entre
                                         -- chacun des éléments d'une liste
intercalate :: [a] -> [[a]] -> [a]       -- Insère des copies d'une liste entre
                                         -- chacune des listes d'une liste
transpose :: [[a]] -> [[a]] -> [a]       -- Retourne la transposée d'une matrice
                                         -- (une matrice étant une liste de listes)
permutations :: [a] -> [[a]]             -- Retourne une liste de permutations
concat :: [[a]] -> [a]                   -- Aplatit une liste de listes en les concaténant
sort :: Ord a => [a] -> [a]              -- Trie une liste
nub :: Eq a => [a] -> [a]                -- Supprime les doublons d'une liste

-- Arithmétique
sum :: Num a => [a] -> a                 -- Retourne la somme des éléments d'une liste
product :: Num a => [a] -> a             -- Retourne le produit des éléments d'une liste
maximum :: Ord a => [a] -> a             -- Retourne le maximum d'une liste
minimum :: Ord a => [a] -> a             -- Retourne le minimum d'une liste

-- Prédicats
isPrefixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est préfixe d'une autre
isSuffixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est suffixe d'une autre
elem :: Eq a => a -> [a] -> Bool         -- Indique si un élément apparaît dans une liste
notElem :: Eq a => a -> [a] -> Bool      -- Indique si un élément n'apparaît pas dans une liste

-- Logique
and :: [a] -> Bool                          -- Retourne vrai si tous les éléments de la liste sont vrais
or :: [a] -> Bool                           -- Retourne vrai s'il existe un élément vrai dans la liste
all :: (a -> Bool) -> [a] -> Bool           -- Retourne vrai si tous les éléments de la liste
                                         -- vérifient une condition donnée
any :: (a -> Bool) -> [a] -> Bool           -- Retourne vrai s'il existe un élément de la liste
                                         -- vérifiant une condition donnée

-- Listes infinies
repeat :: a -> [a]                       -- Répète un élément à l'infini
cycle :: [a] -> [a]                      -- Répète les éléments d'une liste à l'infini
```

## 2 - Manipulation de listes

Récupérez le fichier [ExerciceListe.hs](ExerciceListe.hs) disponible dans le
dépôt et chargez-le dans l'interpréteur Haskell à l'aide de la commande:
```haskell
:l ExerciceListe.hs
```
Aussi, chargez les modules `Data.List` et `Data.Char` en tapant
```haskell
import Data.Char
import Data.List
```
ou : 
```haskell
:m  Data.Char
:m +  Data.List -- (:m - NomModule) pour enlever le module importé
```
En utilisant les fonctions suggérées entre parenthèses, répondez aux questions
suivantes. __Remarque__: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, vous êtes encouragés à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions. Si vous ne
vous rappelez plus de ce que fait la fonction, il peut toujours être utile
d'étudier sa signature avec la commande
```
:t nomFonction
```
Aussi, essayez d'obtenir la réponse en écrivant l'expression en **une seule
ligne**.

- (`sort`) Triez la liste `nombres` (disponible dans `ExerciceListe.hs`).
- (`nub`) Transformez la liste `nombres` de sorte qu'elle ne contienne aucun
  doublon.
- (`minimum`, `maximum`) Calculez l'étendue de la liste `nombres`. En
  statistiques, l'*étendue* d'une liste de nombre est la différence entre la
  valeur maximale et la valeur minimale.
- (`odd`, `filter`, `take`) Extraire les 10 premiers nombres de la liste
  `nombres` qui sont impairs.
- (`mod`, `div`) Produisez le couple `(q,r)` où `q` est le quotient et `r` le
  reste de la division de `147457` par `1297`.
- (`sum`) Montrez que la somme des entiers de 0 à 100 est égale à `100 * 101 / 2`.
- (`length`, `permutations`, `factorial`) Montrez que le nombre de permutations
  de la liste `[1..8]` est bien égal à `8!` (le point d'excalamation se lit
  "factoriel").
- (`transpose`) Montrez que la matrice `matrice` est symétrique (les valeurs
  sont invariantes quand on applique une réflexion par rapport à la diagonale).
- (`toLower`, `map`) Transformez `phrase` en lettre minuscule.
- (`toLower`, `filter`, `map`, `isLower`) Transformez `phrase` en minuscule et
  en supprimant les espaces et les signes de ponctuation.
- (`toLower`, `filter`, `map`, `isLower`, `reverse`) Montrez que `phrase` est
  une phrase palindromique, c'est-à-dire qu'on obtient la même phrase qu'on la
  lise de gauche à droite ou de droite à gauche, en supposant qu'on ignore la
  casse et les signes de ponctuation.
- (`filter`, `isSpace`, `length`) Compter le nombre d'espaces dans `phrase`.
- (`filter`, `isUpper`, `length`) Compter le nombre de lettres majuscules dans
  `phrase`.
- (`all`, `take`, `length`, `drop`, `isUpper`, `isAlpha`, `isDigit`) Montrez
  que `code` est bien un code permanent valide (4 lettres majuscules, suivies
  de 8 chiffres).



## 3- Écriture de fonctions
__Remarque__ : Vous devez spécifier le type de chacune des fonctions que vous écrivez. Vous devez également proposer 2 versions : une version récursive et une version utilisant les listes de compréhension, pour les fonctions marquées
avec `**`. 

### compteurFiltre `**`
Déefinissez la fonction `compteurFiltre` qui prend un critère (fonction de comparaison) et une liste, et renvoie le nombre d'éléments qui répondent au critère. Par exemple :
```haskell
Main> compteurFiltre (==3) [1,2,3,4,5,4,3,2,1]
2
Main> compteurFiltre (>3) [1,2,3,4,5,4,3,2,1]
3
Main> compteurFiltre (<3) [1,2,3,4,5,4,3,2,1]
4
Main> compteurFiltre odd [1,2,3,4,5,4,3,2,1]
5
Main> compteurFiltre (=='e') "Bonjour, comment ça va?"
1
```

### elem' `**`
Définissez la fonction `elem'` (car `elem` existe déjà) qui prend un élément et une liste et renvoie `True` si l'élément fait partie de la liste ou `False` sinon. Par exemple:
```haskell
Main> elem' 3 [1,2,3,4,5,4,3,2,1]
True
Main> elem' 32 [1,2,3,4,5,4,3,2,1]
False
Main> elem' 5 []
False
Main> elem' 'a' "Bonjour, comment ça va?"
True
Main> elem' 'x' "Bonjour, comment ça va?"
False
```

### ensemble `**`
Déefinissez la fonction `ensemble` qui prend une liste et renvoie un ensemble contenant les mêmes éléments (liste sans doublons). Vous ne devez pas utiliser la fonction `nub`. Par exemple :
```haskell
Main> ensemble [1,2,3]
[1,2,3]
Main> ensemble [1,2,3,4,5,4,3,2,1]
[5,4,3,2,1]
Main> ensemble "Bonjour, comment ça va?"
"Bjur,coment\129\231 va?"
Main> :type ensemble
ensemble :: Eq a => [a] -> [a]
```

### intersection `**`
Définissez la fonction `intersection` qui prend deux ensembles et renvoie l'ensemble intersection des deux ensembles. Par exemple:
```haskell
Main> intersection [1,2,3] [3,4,5]
[3]
Main> intersection [1,2,3,4,5,6,7] [3,4,5,6,7,8,9,0]
[3,4,5,6,7]
Main> intersection (ensemble "Bonjour, comment ça va?") (ensemble "Vive Haskell!!!")
"e va"
Main> intersection [pi, 2*pi] [pi/2, pi, 3*pi/2]
[3.14159]
Main> :type intersection
intersection :: Eq a => [a] -> [a] -> [a]
```

### ensemblesEgaux
Définissez la fonction `ensemblesEgaux` qui prend deux ensembles et renvoie `True` s'ils contiennent les mêmes éléments ou `False` sinon.  Par exemple:
```haskell
Main> ensemblesEgaux [1,2,3] [3,2,1]
True
Main> ensemblesEgaux [1,2,3] [3,2,1,0]
False
Main> ensemblesEgaux "Euclide" "cdeEilu"
True
Main> :type ensemblesEgaux
ensemblesEgaux :: Eq a => [a] -> [a] -> Bool
```

### supprimerPremier
Définissez la fonction `supprimerPremier` qui prend un élément et une liste et renvoie la liste sans la première instance de l'élément. Par exemple :
```haskell
Main> supprimerPremier 2 [1,2,3]
[1,3]
Main> supprimerPremier 2 [1,2,2,3,2]
[1,2,3,2]
Main> supprimerPremier 'b' "Bonjour mon beau bonhomme"
"Bonjour mon eau bonhomme"
Main> :type supprimerPremier
supprimerPremier :: Eq a => a -> [a] -> [a]
```
