% 1.1
aplatir([],[]).
aplatir([H|T], [H|R]) :- \+ is_list(H), aplatir(T, R), !.
aplatir([H|T], R) :- aplatir(H, T2), aplatir(T, R1), append(T2, R1, R), !.

% 1.2
compresser([],[]).
compresser([H1],[H1]).
compresser([H1,H2|T], [H1|R]) :- H1 \= H2, compresser([H2|T], R), !.
compresser([_|T], R) :- compresser(T, R), !.

% 2
couleur(rouge).
couleur(jaune).
couleur(vert).
couleur(bleu).

carte(C1,C2,C3,C4,C5,C6) :-
  couleur(C1), couleur(C2), couleur(C3), couleur(C4), couleur(C5), couleur(C6),
  C1 \= C2, C1 \= C3, C1 \= C5, C1 \= C6,
  C2 \= C3, C2 \= C4, C2 \= C5, C2 \= C6,
  C3 \= C4, C3 \= C6,
  C5 \= C6.

% 5 -- Menu d'aliments

% On déclare qu'aliment est dynamique pour pouvoir faire des assert et des retract.
:- dynamic aliment/4.

% Quelques aliments de base
aliment(pomme, fruit, sucre, rouge).
aliment(orange, fruit, sucre, orange).
aliment(cerise, fruit, sucre, rouge).
aliment(banane, fruit, sucre, jaune).
aliment(navet, legume, amer, blanc).
aliment(tomate, fruit, sucre, rouge).
aliment(carotte, legume, sucre, orange).
aliment(laitue, legume, amer, vert).
aliment(citron, fruit, acidule, jaune).
aliment(lime, fruit, acidule, vert).
aliment('chou de Bruxelles', legume, amer, vert).
aliment(ketchup, condiment, acidule, rouge).
aliment(relish, condiment, sucre, vert).

% Déclaration du menu
menu :- prompt(_, ''), % Pour enlever le |: avant les inputs
        write('<== Menu de gestion des aliments ==>'), nl,
        write('1. Visualiser un aliment'), nl,
        write('2. Ajouter un aliment'), nl,
        write('3. Retirer un aliment'), nl,
        write('4. Lister les aliments'), nl,
        write('5. Lister les aliments d\'un certain gout'), nl,
        write('0. Quitter'), nl,
        write('Entrez un choix: '), read(X), nl,
        choix(X).
choix(1) :- write('Nom de l\'aliment a visualiser: '), read(X), nl,
            visualiser(X), menu.
choix(2) :- write('Nom du nouvel aliment: '), read(X), nl,
            ajouter(X), menu.
choix(3) :- write('Entre le nom de l\'aliment a enlever : '), read(X), nl,
            enlever(X), menu.
choix(4) :- aliments(L), write(L), nl, nl, menu.
choix(5) :- write('Entrez un gout :'), read(X), nl,
            choixDeGout(X), menu.
choix(0) :- write('Bye bye.'), nl.
choix(_) :- write('Sélection invalide.'), nl, nl,
            menu.

% Choix 1: Visualiser
visualiser(X) :- aliment(X, T, G, C), !,
                 write('Type : '), write(T), nl,
                 write('Gout : '), write(G), nl,
                 write('Couleur : '), write(C), nl, nl.
visualiser(_) :- write('Aliment inexistant'), nl, nl.

% Choix 2: Ajouter
ajouter(X) :- aliment(X,_,_,_), !,
              write('Cet aliment existe deja. Choix invalide.'), nl, nl.
ajouter(X) :- write('Type : '), read(T),
              write('Gout : '), read(G),
              write('Couleur : '), read(C),
              assert(aliment(X,T,G,C)),
              write('Aliment ajoute.'), nl, nl.

% Choix 3: Enlever
enlever(X) :- aliment(X,_,_,_), !, visualiser(X),
              write('Retirer cet aliment? (o/n)'), read(R), nl,
              retirer(R, X), nl.
enlever(_) :- write('Aliment inexistant.'), nl, nl.
retirer('o', X) :- write('Aliment retire.'), nl, retract(aliment(X,_,_,_)).
retirer('n', _) :- write('L\'aliment n\'a pas été retiré.'), nl.
retirer(_, X) :- write('Caractere invalide. '),
                 write('Retirer cet aliment? (o/n)'), read(R), nl,
                 retirer(R, X).

% Choix 4: Lister
aliments(L) :- findall(X, aliment(X,_,_,_), L).

% Choix 5: Lister un certain gout
choixDeGout(X) :- aliments_de_gout(X, L),
                  write(L), nl, nl.
aliments_de_gout(G, L) :- findall(X, aliment(X,_,G,_), L).
