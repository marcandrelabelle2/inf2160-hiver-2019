% On déclare qu'aliment est dynamique pour pouvoir faire des assert et des retract. 
:- dynamic aliment/4.

aliment(pomme, fruit, sucre, rouge).
aliment(orange, fruit, sucre, orange).
aliment(cerise, fruit, sucre, rouge).
aliment(banane, fruit, sucre, jaune).
aliment(navet, legume, amer, blanc).
aliment(tomate, fruit, sucre, rouge).
aliment(carotte, legume, sucre, orange).
aliment(laitue, legume, amer, vert).
aliment(citron, fruit, acidule, jaune).
aliment(lime, fruit, acidule, vert).
aliment('chou de Bruxelles', legume, amer, vert).
aliment(ketchup, condiment, acidule, rouge).
aliment(relish, condiment, sucre, vert).

% Déclaration du menu
menu :- write('<== Menu de gestion des aliments ==>'),nl, write('1. Visualiser un aliment'), nl, write('2. Ajouter un aliment'), nl, write('3. Retirer un aliment'), nl, write('4. Lister les aliments'), nl, write('5. Lister les aliments d\'un certain gout'),nl, write('0. Quitter'), nl, write('Entrez un choix:'), nl, read(X), choix(X).
choix(0) :- write('Bye bye.'),nl.
choix(1) :- write('Nom de l\'aliment a visualiser: '),nl, read(X), visualiser(X), menu.
choix(2) :- write('Nom du nouvel aliment: '),nl, read(X), ajouter(X), menu.
choix(3) :- write('Entre le nom de l\'aliment a enlever : '),nl, read(X), enlever(X), menu.
choix(4) :- aliments(L), write(L),nl, menu.
choix(5) :- write('Entrez un gout :'),nl, read(X), choixDeGout(X), menu.
choix(_) :- write('Sélection invalide.'),nl, menu.