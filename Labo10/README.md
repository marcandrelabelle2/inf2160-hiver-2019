# Listes, Puzzle logique, Base de connaissances, Entrées/Sorties, Structures
## 1- Listes 
#### 1.1- Définissez le prédicat `aplatir(L1,L2)`, qui transforme une liste **L1** (pouvant éventuellement contenir des listes) en une liste **"plate"** en remplaçant chaque liste par ses éléments (récursivement). 
**Indice**: Vous pouvez utiliser le prédicat prédéfinit `is_list/1`.
```prolog 
?- aplatir([a,[b,[c,d],e]],X).
X = [a, b, c, d, e].
?- aplatir(3,X).
X = [3].
```
#### 1.2- Définissez le prédicat `compresser(L1,L2)`, qui élimine les doublons consécutifs dans une liste **L1**.
 Si une liste L1 contient des éléments répétés, ils doivent être remplacés par une seule occurrence de l'élément. L'ordre des éléments ne doit pas être modifié.
```prolog
?- compresser([a,a,a,a,b,c,c,a,a,d,e,e,e,e],X).
X = [a, b, c, a, d, e] .
```

## 2- Puzzle logique 
On dispose de 4 couleurs (rouge, jaune, vert, bleu) pour colorier la carte représentée ci-dessous.

![](cassetete.PNG "Cassetête")

Écrire un programme Prolog qui permet d’associer une couleur (rouge, jaune, vert, bleu) à une région (1, 2, 3, 4, 5, 6) de telle manière que deux régions adjacentes ne soient pas de la même couleur. Par exemple :

```prolog
?- carte(C1,C2,C3,C4,C5,C6).
C1 = C4, C4 = rouge,
C2 = jaune,
C3 = C5, C5 = bleu,
C6 = vert ;
C1 = rouge,
C2 = jaune,
C3 = C5, C5 = bleu,
C4 = C6, C6 = vert
```

## 3- findall/3, bagof/3, setof/3

Nous partons de l'ensemble de faits suivant donnant l'age de 6 personnes :
```prolog
age(vincent, 28).
age(marie, 25).
age(alexis, 23).
age(laure, 23).
age(alice, 26).
age(paul, 21).
``` 
#### 3.1- Écrire une requête qui permet d’afficher une liste contenant les âges.
```prolog
L = [28, 25, 23, 23, 26, 21]
``` 

* Que fait `bagof(X,age(_,X),L)`, `bagof(X,age(Y,X),L)` ?
* Écrire une requête avec `bagof/3`, qui retourne le même résultat que la requête 1 (une liste contenant les âges).
* En utilisant `bagof/3`, écrivez une requête qui affiche la liste des prénoms sans afficher les âges.
* Que fait `setof(X,age(_,X),L)`, `setof(X,age(Y,X),L)`, `setof(X,Y^age(Y,X),L)`, `setof(Y,X^age(Y,X),L)`?
* Le prédicat `list_to_set/2` supprime les doublons mais n'ordonne pas les éléments de la liste. Que fait `findall(X,age(Y,X),L1),list_to_set(L1,L2)` ?

**Résumé**: `bagof/3` est similaire à `findall/3` sauf qu'il peut fournir les résultats dans des listes
séparées. `setof/3` est similaire à `bagof/3` sauf qu'il peut fournir une liste unique de résultats ordonnées et sans doublons. `setof/3` est comparable à `findall/3` suivi de `sort/2`.

## 4- Structures : Ce qu'il faut savoir ...
On peut définir des structures en Prolog. Les structures n'ont pas besoin d'être définies à l'avance, puisqu'il n'y a pas de types en Prolog, et que la résolution se fait par unification. **Exemple** :
```prolog
acteur('Schawrznnegger Arnold',ma,1000,date(1978,4,1),arnold).
acteur('Streep Meryll',fe,1500,date(1984,9,1),merryl).
``` 
**Utilisation** : Prédicat qui retourne le nombre d’année d’expérience d’un acteur (Le temps écoulé entre l’année de début de sa carrière et l’année 2012)
```prolog
nombre_annees_experience2012(A,Na) :- acteur(_,_,_ ,date(Adebut,_,_),A), Na is 2012 - Adebut.
| ?- nombre_annees_experience2012(merryl,Na).
Na = 28.
| ?- nombre_annees_experience2012(arnold,Na).
Na = 34.
``` 

## 5- Entrées/Sorties et modification de la BC

Prédicat utile pour cette partie : `member(E,L)` définit l’appartenance d’un élément E à une liste L. Exemple :
```prolog
| ?- member(3,[1,2,3]).
true.
``` 
Si le prédicat n’est pas reconnu, rajouter la ligne suivante dans votre base de faits
```prolog
:- use_module(library(lists)). % importation de la librairie lists
``` 

Créez un petit programme Prolog qui permet de gérer des aliments. Vous devez compléter un menu (voir fichier `labo10_menu.pl`) par lequel on peut **visualiser** un aliment, en **ajouter** un, en **retirer** un, **voir** la liste de tous les aliments ou voir la liste des aliments d'un certain goût.

Voici un scénario type de manipulation de cette BC:
```prolog
| ?- menu.
<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 1.
Nom de l'aliment a visualiser:
|: pomme.
Type: fruit
Gout: sucre
Couleur: rouge

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 2.
Nom du nouvel aliment:
|: pomme.
Cet aliment existe deja. Choix invalide.

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 2.
Nom du nouvel aliment:
|: biere.
Type: |: boisson.
Gout: |: amer.
Couleur: |: jaune.
Aliment ajoute.

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 3.
Nom de l'aliment a retirer:
|: pomme.
Type: fruit
Gout: sucre
Couleur: rouge
Retirer cet aliment? (o/n)
|: k.
Caractere invalide. Retirer cet aliment? (o/n)
|: o.
Aliment retire.

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 3.
Nom de l'aliment a retirer:
|: pomme.
Cet aliment n'existe pas.

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 4.
[orange, cerise, banane, navet, tomate, carotte, laitue, citron, lime, chou de Bruxelles,
ketchup, relish, biere].

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 5.
Quel gout?
|: amer.
[navet, laitue, chou de Bruxelles, biere].

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 3.
Nom de l'aliment a retirer:
|: biere.
Type: boisson
Gout: amer
Couleur: jaune
Retirer cet aliment? (o/n)
|: o.
Aliment retire.

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 5.
Quel gout?
|: amer.
[navet, laitue, chou de Bruxelles].

<== Menu de gestion des aliments ==>
1. Visualiser un aliment
2. Ajouter un aliment
3. Retirer un aliment
4. Lister les aliments
5. Lister les aliments d'un certain gout
0. Quitter
Entrez un choix:
|: 0.
Bye bye.
``` 