# Paradigmes de programmation

Ce dépôt contient les énoncés et solutions *partielles* des laboratoires du cours INF2160 Paradigmes de
programmation, enseigné à l'UQAM pour l'hiver 2019. 

## Haskell
* [Labo 1](Labo1) ([Solution](https://gitlab.com/angetato/inf2160-hiver-2019/blob/master/Labo1/Labo1_solution.hs))
* [Labo 2](Labo2) ([Solution](https://gitlab.com/angetato/inf2160-hiver-2019/blob/master/Labo2/Labo2_solution.hs))
* [Labo 3](Labo3) ([Solution](https://gitlab.com/angetato/inf2160-hiver-2019/blob/master/Labo3/Labo3_solution.hs))
* [Labo 4](Labo4) ([Solution](https://gitlab.com/angetato/inf2160-hiver-2019/blob/master/Labo4/Labo4_solution.hs))
* [Labo 5](Labo5) ([Solution](Labo5/Labo5_solution.md))
* [Labo 6](Labo6) ([Solution](Labo6/Labo6_menu_solution.hs))
* [Préparation Examen Intra](PreparationIntra) (__Pas de solution, si questions contactez nous !__)


### Commandes utiles sous haskell:
* `{- -}` : pour des commentaires sur plusieurs lignes
* `--` : pour un commentaire sur une ligne
* `:l fichier.hs` : pour exécuter votre script fichier.hs sous ghci
* `CTRL-D` ou la commande `:q` : pour quitter l'invite ghci
* `:bro Prelude` : pour afficher les fonctions disponibles dans Prelude


## Prolog
* [Labo 7](Labo7) ([Solution](Labo7/solutions/))
* [Labo 8](Labo8) ([Solution](Labo8/solution.pl))
* [Labo 9](Labo9) ([Solution](Labo9/solution.pl))
* [Labo 10](Labo10) ([Solution](Labo10/solution.pl))
* [Labo 11](Labo11) (Solution)



### Commandes utiles sous prolog:
* `/* */` : pour des commentaires sur plusieurs lignes;
* `%` : pour un commentaire sur une ligne;
* `swipl fichier.pl` : pour exécuter votre script fichier.pl à l'extérieur de l'invite prolog;
* `['fichier.pl']` ou `[fichier]` : pour exécuter votre script fichier.pl une fois dans l'invite prolog;
* `edit.` : pour modifier le script déjà chargé;
* `halt.` : pour quitter l'invite;
* `listing(nom_predicat)`: Pour afficher le code source du prédicat dans l'invite de commande.


## Contacts
Courriel **Jaël** (Groupe _Bruno_) :
champagne_gareau.jael@courrier.uqam.ca

Courriel **Ange** (Groupes _Bruno_ & _Roger_) :
nyamen_tato.ange_adrienne@courrier.uqam.ca
