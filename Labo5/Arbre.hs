module Arbre where

-- | Un arbre binaire de recherche classique
data ABR a = ABRVide | Noeud a (ABR a) (ABR a)
    deriving (Show)

-- | Insertion
inserer :: Ord a => a -> ABR a -> ABR a
inserer x ABRVide = Noeud x ABRVide ABRVide
inserer x (Noeud y g d)
    | x == y = Noeud x g d
    | x < y  = Noeud y (inserer x g) d
    | x > y  = Noeud y g (inserer x d)

-- | Insertions multiples
insererPlusieurs :: Ord a => ABR a -> [a] -> ABR a
insererPlusieurs = foldl (flip inserer)

-- | Appartenance
appartient :: Ord a => a -> ABR a -> Bool
appartient _ ABRVide = False
appartient x (Noeud y g d)
    | x == y = True
    | x < y  = appartient x g
    | x > y  = appartient x d

-- | Nombre de noeuds
--
-- Note: Un arbre vide n'est pas un noeud.
nbNoeuds :: ABR a -> Int
nbNoeuds ABRVide = 0
nbNoeuds  (Noeud _ g d) = 1 + nbNoeuds g + nbNoeuds d

-- | Nombre de feuilles
--
-- Note: Une feuille est un noeud dont les deux sous-arbres sont vides.
nbFeuilles :: ABR a -> Int
nbFeuilles (Noeud _ ABRVide ABRVide) = 1
nbFeuilles (Noeud _ g d) = 1 + nbFeuilles g + nbFeuilles d

-- | Hauteur
--
-- Note: Un arbre vide est de hauteur 0, alors qu'une feuille est de hauteur 1.
hauteur :: ABR a -> Int
hauteur ABRVide = 0
hauteur (Noeud _ g d) = 1 + max (hauteur g) (hauteur d)

-- | Est-ce que l'arbre est équilibré?
--
-- Un arbre est équilibré si la différence de hauteur entre *chacun* de ses
-- sous-arbres gauche et droite diffère d'au plus 1.
estEquilibre :: ABR a -> Bool
estEquilibre (Noeud _ ABRVide d)= hauteur d <= 1
estEquilibre (Noeud _ g ABRVide)= hauteur g <= 1
estEquilibre (Noeud _ g d)= (abs (hauteur g - hauteur d) <= 1) && estEquilibre g && estEquilibre d
