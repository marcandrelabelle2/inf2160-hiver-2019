module Image where
import Data.List.Split

-- | Une couleur représentée par son niveau de rouge, vert et bleu
data Couleur = RGB Int Int Int deriving (Show)

-- | Une image
data Image = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[Couleur]]
} deriving (Show)

-- | Indique si la hauteur et la largeur de l'image sont cohérentes avec la
-- matrice de pixels
estValide :: Image -> Bool
estValide (Image h l p@(x:xs)) = length p == h && length x == l

-- | Retourne une image de dimensions données dont tous les pixels sont de même
-- couleur
imageUnicolore :: Int     -- Hauteur
               -> Int     -- Largeur
               -> Couleur -- La couleur
               -> Image   -- L'image résultante
imageUnicolore h l c = Image h l pixels 
                       where
                          pixels = replicate h (replicate l c)

-- | Retourne une image de dimensions données en alternant les couleurs données
-- comme dans un échiquier.
echiquier :: Int     -- Hauteur
          -> Int     -- Largeur
          -> Couleur -- Première couleur
          -> Couleur -- Deuxième couleur
          -> Image   -- L'image résultante
echiquier h l c1 c2 = Image h l p
                      where p  = take h (cycle [lp, li])
                            lp = take l (cycle [c1, c2])
                            li = take l (cycle [c2, c1])
