# Exercices de préparation à l'Intra (extrait des examens 2015-2018)
## 1. Types et définitions de fonctions générales

* Définir la fonction `dernierDevientPremier` (en précisant son type) qui ramène le dernier élément d’une liste en tête de la liste et retourne la liste résultante. Par exemple :
    ```haskell}
       ghci> dernierDevientPremier [2,5,7,9]
       [9,2,5,7]
       ghci> dernierDevientPremier ["je","tu","il"]
       ["il","je","tu"]
    ```
* Définir la fonction `substituerImpaire :: [Int] -> [Int]` qui remplace chaque élément impaire de la liste passée en paramètre par son suivant dans le type Int. Par exemple :
    ```haskell
       ghci> substituerImpaire [3,2,5,7,4]
       [4,2,6,8,4]
    ```
* Définir la fonction `suffixes :: [a] -> [[a]]` qui prend une liste et en construit une autre comportant tous les suffixes de la première liste. Une liste xs est le suffixe d’une liste ys, si la liste ys se termine par les éléments de xs (dans l’ordre). Par exemple :
    ```haskell
       ghci> suffixes "abra"
       ["abra","bra", "ra","a", ""]
    ```
* Définir la fonction `contient` (préciser son type) qui, étant données deux listes, vérifie si la deuxième liste est une sous-liste de la première. Une liste xs est une sous-liste de la liste ys si ys peut être réécrite comme étant as++xs++bs, avec as et bs des listes quelconques. **Remarque** : l’usage de la fonction `suffixe` précédente peut être utile. Par exemple :
    ```haskell
       ghci> "abracadabra" `contient` "cad"
       True
       ghci> "abracadabra" `contient` "radab"
       False
    ```
* Définir la fonction `groups :: Int -> [a] ->[[a]]` qui, étant donné un nombre k et une liste, produit une nouvelle liste constituée de sous-listes de longueur k (comportant dans l’ordre les éléments de la liste initiale). Par exemple :
   ```haskell
       ghci> groups 2 [3,1,4,1,5,9]
       [[3,1],[4,1],[5,9]]
       ghci> groups 3 "SEND MORE YOGURT"
       ["SEN","D M","ORE"," YO","GUR","T"]
    ```
* En utilisant `foldr` ou `foldl`, écrire la fonction `ungroups` (préciser son type) qui produit une liste constituée d’éléments issue de tous les sous-listes dans la liste passée en paramètre. Par exemple :
    ```haskell
       ghci> groups ["SEN","D M","ORE"," MO","NEY"]
       "SEND MORE MONEY"
* Que va afficher le code suivant :
    ```haskell
       fa x | x < 2 = error ""
            | otherwise = filter ( ez . m ) [1..x]
                          where
                             m = mod x
                             ez = (==) 0
       main = print ( fa 6 )
    ```
* Lors de l’exécution du code ci-dessous, si l’utilisateur tape au clavier la chaîne `"1534"`, que va afficher le programme?
    ```haskell
       s :: IO Bool -> IO () -> IO () -> IO ()
       s p i1 i2 = do t <- p
                      if t then do i1
                        return ()
                      else do i2
                        return ()
       main = do r <- getLine
                 s ( return ( ( head r ) == '1' ) ) 
                 ( putStrLn ( tail r ) )
                 ( putStrLn ( reverse r )
    ```
* Soit le code suivant :
```haskell
data Liste a = Vide | Cons a (Liste a) deriving Show
instance Functor Liste where
    fmap f Vide = Vide
    fmap f (Cons h t) = Cons (f h) (fmap f t)

instance Applicative Liste where
    pure x = Cons x $ pure x
    _ <*> Vide = Vide
    Vide <*> _ = Vide
    Cons f fs <*> Cons x xs = Cons (f x) $ fs <*> xs
lEntiers = Cons 1 (Cons (-5) (Cons 3 Vide ))
lFonctions = Cons (+ 1) (Cons (*2) (Cons (+2) Vide) )
```
Pour chacune des expressions suivantes,  ́ecrire la  ́eponse que va produire ghci.
```haskell
- [(*2),(+4)] <*> [1,2,3]
- lFonctions <*> lEntiers
- pure (*4) <*> lEntiers
- (fmap (+) lEntiers ) <*> lEntiers
- (fmap (*) lEntiers ) <*> lEntiers
```

## 2. Application
**Note**: De nombreuses questions dans cette partie font référence aux définitions que comporte le module `Hollywood.hs` que l’on supposera déjà chargé dans GHCi via le fichier `donnees.hs`. Vous ne pouvez donc utiliser que ce qui est accessible dans le cadre de ce module.
### 2.1 Exécution Haskell
Les réponses à donner pour cette sous-partie doivent fournir, comme Haskell ferait, la valeur du résultat ou, le cas échéant, la nature de l’incident ou de l’erreur.
* Quelle est la réponse fournie par Haskell après l’évaluation de l’expression suivante?
    ```haskell
       restriction sharon nuit
   ```
* Quelle est la réponse fournie par Haskell après avoir évalué l’expression suivante?
    ```haskell
       map titreFilm (filter (\f -> typeFilm f == Action) (listeFilmsActeur chris))
    ```

### 2.2 Utilisation de constructeurs et fonctions Haskell pour répondre à des requêtes}
Pour les questions suivantes, vous devez répondre en donnant l’expression Haskell retournant le résultat
requis. Cette expression ne doit faire appel qu’à des fonctions, fonctionnelles, opérateurs ou constructeurs définis en annexe ou prédéfinis en Haskell. **Vous ne devez pas inventer ou utiliser de nouvelles fonctions**.
* Comment obtenir la liste des `titres de films` satisfaisant aux restrictions de l’actrice `sharon`?
* Comment produire la liste des acteurs (`leur nom`) qui ont joué dans le film `"Iron Man"` ?
* En utilisant `foldr` ou `foldl`, écrivez une fonction qui retourne le `budget total` de tous les films (sachant que la fonction `listeFilms` retourne la liste de tous les films)?
