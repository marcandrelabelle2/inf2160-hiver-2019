import Hollywood
-- Les réalisateurs
woody = (Realisateur "Allen Woody" "New York New York")
denis = (Realisateur "Arcan Denis" "Montreal Quebec")
joss = (Realisateur "Joss Whedon" "New York -")
-- Les acteurs et leurs restrictions
arnold = (Acteur "Schwarzennegger Arnold" M (\(Film titreF typeF realisateurF dureeF
nbacteursF budgetF) -> typeF == Action && realisateurF /= PasDeRealisateur) [])
julia = (Acteur "Roberts Julia" F (\(Film titreF typeF realisateurF dureeF nbacteursF budgetF)
-> (typeF == Drame || typeF == Humour) && ( budgetF < 4000)) [])
sharon = (Acteur "Stone Sharon" F (\(Film titreF typeF realisateurF dureeF nbacteursF budgetF)
-> ((length titreF) > 4 && (length titreF) < 150) && realisateurF /= woody ) [])
robert = (Acteur "Robert Downey Jr" M ( \(Film titreF typeF realisateurF dureeF nbacteursF
budgetF) -> budgetF >= 100000 ) [iron, sherlock])
chris = (Acteur "Chris Evans " M (\(Film titreF typeF realisateurF dureeF nbacteursF budgetF)
-> typeF == Action) [iron])
--liste des acteurs
listeActeurs = [arnold, julia, sharon, robert, chris]
-- Les films
destruction = (Film "Destruction" Action PasDeRealisateur 120 150 60000)
nuit = (Film "Une nuit a Casablanca" Humour woody 110 24 1000)
coupe = (Film "Coupe de coupe !" Horreur denis 45 2 10000)
wind = (Film "Gone with the wind !" Drame woody 120 4 100 )
iron = (Film "Iron Man" Action joss 126 3 100000)
sherlock = (Film "Sherlock Holmes" Policier joss 128 1 90000 )
--liste des films
listeFilms = [destruction, nuit, coupe, wind, iron, sherlock]