module Hollywood where

type Nom = String
type Adresse = String
type Budget = Int
type Titre = String
type Duree = Int
type NombreActeurs = Int

-- Un réalisateur est défini par son nom et son adresse
data Realisateur = PasDeRealisateur | Realisateur Nom Adresse deriving (Show, Eq)
-- Il y a 5 types de film
data TypeF = Action | Drame | Humour | Horreur | Policier deriving (Show, Eq)
-- Un Film se caractérise par son titre, son type, son réalisateur, sa durée, 
-- le nombre d'acteurs, son budget
data Film = Film Titre TypeF Realisateur Duree NombreActeurs Budget
type Restriction = Film -> Bool
data Sexe = M | F deriving (Eq)
-- Un acteur est définit par son nom, son sexe, ses restrictions et la liste des films 
-- dans lesquels il a joué
data Acteur = Acteur Nom Sexe Restriction [Film]
-- la fonction titreFilm retourne le titre du film passé en paramètre
titreFilm :: Film -> Titre
titreFilm (Film titreF _ _ _ _ _) = titreF
-- la fonction typeFilm retourne le type du film passé en paramètre
typeFilm (Film _ typeF _ _ _ _) = typeF
-- la fonction budgetFilm retourne le budget du film passé en paramètre
budgetFilm (Film _ _ _ _ _ b) = b
-- la fonction nomActeur retourne le nom d'un acteur.
nomActeur (Acteur nom _ _ _) = nom
-- la fonction restriction retourne la composante restriction de l'argument de type acteur
restriction (Acteur _ _ r _) = r
-- la fonction restriction retourne la liste des films d'un acteur
listeFilmsActeur(Acteur _ _ _ lf) = lf