# Listes, Coupure, Base de connaissances
## 1- Opérateurs 
#### Que retourne chacune de ces expressions ?

```prolog 
a) X=1 + 2.
b) X is 1 + 2.
c) X = 3, X == 1 + 2.
d) X is 3, X==3.
e) 10 = 5 + 5.
f) 10 =:= 5 + 5.
g) Y is 10, Y is 8.
```

## 2- Coupure (!) et Listes

#### 2.1- Considérez le prédicat suivant : 
```prolog
signe(N, plus) :- N > 0. 
signe(0, zero).
signe(N, moins) :- N < 0.
```
Réécrivez-le à l'aide de coupures, de telle sorte qu'il ait exactement le même comportement, mais qu'il soit plus efficace (il ne fait aucune vérification inutile).

#### 2.2- Sans utiliser de coupures, implémentez le prédicat `separer(L, P, N)` qui retourne vrai si et seulement si les valeurs de P et les valeurs de N sont respectivement les valeurs positives et négatives de la liste L. Par exemple :
```prolog
?- separer([2,-1,4,7,-8,0,-2], P, N). 
P = [2, 4, 7, 0]
N = [-1, -8, -2]
```
Ensuite, optimisez votre prédicat à l'aide de coupures qui ne modifient pas le résultat, mais seulement l'efficacité du calcul.

#### 2.3- Définissez le prédicat `tailleListe(Liste, Taille)` qui calcul la longueur d'une liste. Ce prédicat réussit si le deuxième argument est la longueur de la liste passée en premier argument.

#### 2.4- Définissez le prédicat `membre(Liste, Element)` qui réussit si le premier argument est membre de la liste passée en deuxième argument. Ne pas utiliser le prédicat **member** définit sous prolog.
```prolog
| ?- membre([12,14],14). 
true
| ?- membre([12,11],10). 
False.
```

#### 2.5- Définissez le prédicat `dernier(Liste, Element)` qui réussit si le deuxième argument est le dernier élément de la liste.
```prolog
| ?- dernier([14,12,14],D). 
D = 14.
| ?- dernier([12,11],11). 
True.
```

#### 2.6- Définissez le prédicat `moyenne(ListeNombre, Resultat)` qui est vrai quand **Resultat** est égale à la moyenne de la **ListeNombre**. Selon les versions de Prolog, il se peut que la division nécessaire à ce prédicat soit une division entière et que votre résultat soit légèrement différent des exemples. Par exemple:
```prolog
| ?- moyenne([12,14],M).
M = 13.0.
| ?- moyenne([3,0,5,8,4,9,8,10,6,7],M). 
M = 6.0.
| ?- moyenne([],M). 
False.
```

#### 2.7- Définissez le prédicat `max(Liste, Max)` qui est vrai si **Max** est égal au plus grand nombre de la **Liste**. Par exemple:
```prolog
| ?- max([-1,-2,-3],X). 
X = -1 .
| ?- max([3,35,6],X). 
X = 35.
| ?- max([3,35,35,6,0,-42],X). 
X = 35.
```


## 3- Gestion d’un compteur
**Prédicats utiles pour répondre à cette deuxième partie**:

* `Recorda/3` : `recorda(Key, Term, Reference)` ajoute `Term` dans la base de connaissances sous la clé `Key`. Key peut être un entier, un atome etc. La `référence` n’est généralement pas utilisée à l’appel du prédicat, mais sera utilisé dans le prédicat `erase/1` ci-dessous. **Exemple 1**:
```prolog
?- recorda(c,3,_). 
yes.
```
* `Recorded/3`: `recorded(Key, Value, Reference)` est vrai si `Value` est enregistrée sous la clé `Key` et a la référence `Reference. **Exemple 2**: Supposons que l’exemple 1 ait été exécuté 
```prolog
?- recorded(c,3,_). 
yes.
?- recorded(c,4,_). 
no.
?- recorded(c,_,Ref).
Ref = '\$ref'(64854094362,95) ? 
yes.
```

* `Erase/1` : `erase(Reference)` efface un enregistrement ou une clause de la base de données. `Référence` est une référence retournée par l'enregistrement recorda/ 3, recordz/3 ou recorded/3, assert/2, asserta/2 ou assertz/2. **Exemple 3** : Supposons que l’exemple 1 ait été exécuté
```prolog
?- recorded(c,_,Ref),erase(Ref). 
Ref = '\$ref'(64854094362,95) ? 
yes.
?- recorded(c,_,Ref).
no.
```

* `Call/1`: `call(Goal)` appelle `Goal` comme but à satisfaire. **Exemple 4**:
```prolog
?- recorded(c,3,_). 
yes.
% Est similaire à
?- call(recorded(c,3,_)). 
yes.
```

On veut pouvoir gérer un compteur comme une variable globale d’un langage impératif. Définir les prédicats suivants :

#### 3.1- `cptInit(Cpt,Val)` : le compteur **Cpt** est initialisé à **Val**. **Remarque** : Val doit absolument être un entier, de plus il faudra s’assurer qu’il n’y ait qu’une seule occurrence (la dernière saisie) d’un même compteur dans la base de connaissances (Ex : on ne peut avoir c=0 puis c= 1 dans la même base).
```prolog
?- cptInit(c,0). 
true.
?- cptInit(d,3). 
true.
?- cptInit(e,4). 
yes
?- cptInit(e,5). 
yes
?- cptVal(e,V). 
V =5 ? ;
no
```

#### 3.2- `cptFin(Cpt)` : le compteur **Cpt** est détruit.
```prolog
?- cptFin(c). 
true.
?- cptFin(d). 
true.
```

#### 3.3- `cptVal(Cpt,Val)` : la valeur du compteur **Cpt** est **Val**.
```prolog
?- cptVal(c,3). 
true.
?- cptVal(c,V). 
V = 3.
```

#### 3.4-  `cptInc(Cpt,Inc)` : le compteur **Cpt** est incrémenté de **Inc**.
```prolog
?- cptVal(c,V).
V = 3.
?- cptInc(c,4), cptVal(c,V). 
V = 7.
?- cptVal(d,V).
V = 6.
?- cptInc(d,-5), cptVal(d,V). 
V = 1.
```

#### 3.5-  En utilisant les éléments définis plus haut, définir le prédicat `alphabet` qui affiche les lettres de l’alphabet en utilisant un compteur et le prédicat `repeat/0`. **Remarque** : Le code ASCII de "a"=97, le code ASCII de "z"= 122 et le code ASCII de " "= 32. Pour afficher un caractère à partir de son code ASCII, utilisez le prédicat **put**. Exemple : **put(97) = a**
```prolog
?- alphabet.
a b c d e f g h i j k l m n o p q r s t u v w x y z 
true.
```
