%Partie 1
/* 
a) X = 1+2.
b) X = 3.
c) False.
d) X = 3.
e) False.
f) True
g) False
*/


%Partie 2: Coupure et Listes
signe(N, plus) :- N > 0.
signe(0, zero).
signe(N, moins) :- N < 0.

signe_(0,zero) :- !.
signe_(N,Signe) :- N > 0, !, Signe = plus.
signe_(_,moins).


separer([],[],[]).
separer([X|XS],[X|PS],NS):- X>=0, separer(XS,PS,NS).
separer([X|XS],PS,[X|NS]):- X<0, separer(XS,PS,NS).

separer_([],[],[]):- !.
separer_([X|XS],[X|PS],NS):- X>=0,!, separer_(XS,PS,NS).
separer_([X|XS],PS,[X|NS]):- separer_(XS,PS,NS).

tailleListe([],0).
tailleListe([_|Q],N):- tailleListe(Q,N1), N is N1 + 1.

membre([X|_],X).
membre([Y|Q],X):- Y \== X, membre(Q,X).

dernier([X],X).
dernier([_|Q],X):- dernier(Q,X).

somme([X],X).
somme([X|Q],S):- somme(Q,S1), S is S1 + X.
moyenne(L,M):- tailleListe(L,N), somme(L,S), M is S / N.


max([M|R],M1) :- max(R,M1), M1 > M,!.
max([M|_],M).
/* une autre version du max
max([T|L],M) :- max(L,T,M), !.
max([],M,M) :- !.
max([T|Q],Temp, M) :- T > Temp, max(Q, T, M).
max([_|Q],Temp, M) :- max(Q, Temp, M).
*/

%Partie 3: Gestion d’un compteur
% cptInit/2
cptInit(Cpt,Val) :-
    killCpt(Cpt), integer(Val), recorda(Cpt,Val,_).
% killCpt /1
killCpt(Cpt) :- recorded(Cpt,_,Ref), erase(Ref), !.
killCpt(_):-!.

% cptFin /2
cptFin(Cpt) :- killCpt(Cpt).

% cptVal /2
cptVal(Cpt,Val) :- recorded(Cpt,Val,_).

% cptInc /2
cptInc(Cpt, Inc) :-
  integer(Inc), recorded(Cpt,Val,Ref), erase(Ref),
  V is Val + Inc, recorda(Cpt,V,_).

% alphabet /0  Remarque: utilisez put à la place de put_code avec SWI_Prolog
alphabet :-
    cptInit(c ,97) ,
    repeat ,
     cptVal(c,V), put_code(V), put_code(32), cptInc(c ,1), 
     V =:= 122 , !,
     cptFin(c).



