## 1 - Curryfication

Quel est la différence entre les deux fonctions suivantes ?  
Est-ce que l'une d'elle est plus avantageuse ?
```haskell
addition1 :: Num a => (a,a) -> a
addition1 (x,y) = x+y

addition2 :: Num a => a -> a -> a
addition2 x y = x+y
```

Analysez le type des fonctions :
```haskell
curry
uncurry
curry addition1
uncurry addition2
```

Pour plus de détails sur la curryfication,
[cliquer ici](https://fr.wikipedia.org/wiki/Curryfication)

## 2 - Opérateur applicatif de fonction 

Analysez le type de la fonction `$`.  
À quoi cette fonction peut-elle servir ?
Indice: [Priorité des opérations](https://rosettacode.org/wiki/Operator_precedence#Haskell)

Une autre utilité est d'appliquer une valeur sur une liste de fonctions.  
Avec les fonctions `(4+), (*10), (^2), sqrt`, la fonction `map` et l'opérateur `$`,
trouvez une commande ayant pour résultat `[8, 40, 16, 2]`.

## 3 - Composition de fonctions

Faites une fonction
```haskell
mystere :: Num a => [[a]] -> [a]
```
en utilisant `negate, sum, tail` qui enlève le premier élément de chaque liste
en entrée, fait la somme de chaque liste et multiplie par -1 chaque résultat.
Par exemple:
```haskell
main> mystere [[1,2,3], [4,5,6]]
[-5, -11] 
```

Regardez la fonction `(.)`. Réécrivez la fonction `mystere` sans parenthèses.

## 4 - Fonctions d'ordre supérieur

À l'aide des matrices suivantes:
```haskell
matriceA = [[ 3,  1, -2],
            [-1,  0,  2],
            [ 4, -1,  5]]
matriceB = [[-1,  4,  2],
            [ 0, -6, -1],
            [ 8,  6, -4]]
```

utilisez les fonctions suggérées entre parenthèses et répondez aux questions
suivantes. *Remarque*: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, je vous encourage à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions.

### 4.1 - Multiplication par un scalaire

(`map`, `.`, `(*5)`) Produisez la matrice obtenue de `matriceA` en multipliant
chaque entrée par `5`. Vous devriez donc obtenir
```haskell
[[15,5,-10],[-5,0,10],[20,-5,25]]
```

(`map`, `.`, `(*)`) Déduisez-en l'implémentation d'une fonction
`multiplicationParScalaire` qui multiplie toutes les entrées d'une matrice par
un scalaire donné, dont la signature est
```haskell
multiplicationParScalaire :: Num a => a -> [[a]] -> [[a]]
```

### 4.2 - Somme de deux matrices

(`zipWith`, `(+)`, `.`) Produisez la somme des matrices `matriceA` et
`matriceB`. Le résultat attendu est
```haskell
[[2,5,0],[-1,-6,1],[12,5,1]]
```

(`zipWith`, `(+)`, `.`) Déduisez-en une fonction
```haskell
sommeMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
qui calcule la somme de deux matrices.

### 4.3 - Produit scalaire

(`sum`, `zipWith`, `(*)`) Définissez une fonction qui calcule le produit
scalaire de deux listes (pas besoin de valider si les longueurs sont égales,
vous pouvez supposer que c'est le cas). La signature est
```haskell
produitScalaire :: Num a => [a] -> [a] -> a
```
On s'attend à ce que le résultat soit le suivant
```haskell
Prelude> produitScalaire [1,2,3] [4,5,6]
32
Prelude> produitScalaire [-1,-3,2] [0,2,4]
2
```

### 4.4 - Produit matriciel

(`produitScalaire`, `transpose`) Définissez une fonction qui calcule le produit
de deux matrices, dont la signature est
```haskell
produitMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
Il y a une façon élégante de faire en observant que le produit de deux matrices
peut être vu comme le produit scalaire de chaque ligne de la première matrice
avec chaque colonne de la deuxième matrice. *Note*: La compréhension de liste
pourrait aussi vous être utile.

## 5 - Jeu de tic-tac-toe

Pour cet exercice, vous devez récupérer le fichier
[TicTacToe.hs](https://gitlab.com/angetato/inf2160-hiver-2019/blob/master/Labo4/TicTacToe.hs) disponible dans ce dépôt.

### 5.1 - Affichage d'une grille

Implémentez une fonction qui permet d'afficher une grille, dont la signature
est
```haskell
showGrille :: Grille -> String
```
qui affiche la grille sous format plus agréable.

Nous aimerions avoir le comportement suivant:
```haskell
Prelude> putStrLn $ showGrille ((X,O,Vide),(X,Vide,O),(X,Vide,Vide))
X | O |
--+---+--
X |   | O
--+---+--
X |   |
```
*Note*: la fonction `putStrLn` permet d'afficher une chaîne de caractère (donc
les guillemets n'y sont plus, et les `\n` sont interprétés comme des retours à
la ligne).

### 5.2 - Réimplémentation de `jouerCoups`

Modifiez l'implémentation de la fonction `jouerCoups` pour qu'elle fasse appel
à la fonction `foldr` (ou `foldl`).

### 5.3 - Réimplémentation de `jouerPartie`

Faites une autre version de la fonction `jouerPartie` pour qu'elle fasse appel
à la fonction `jouerCoups`. Il pourrait être intéressant d'utiliser la fonction
`foldr` (ou `foldl`), la fonction `zip3` et l'expression `cycle [X,O]`.
NOTE: Pas besoin de traiter les erreurs et les parties gagnantes et perdantes.
