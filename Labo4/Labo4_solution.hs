import Data.List

-- Section 2

section2 = map ($ 4) [(4+), (*10), (^2), sqrt]

-- Section 3

mystere :: Num a => [[a]] -> [a]
mystere xxs = map (\xs -> negate(sum(tail(xs)))) xxs
mystere' xxs = map (negate . sum . tail) xxs

-- Section 4

matriceA = [[ 3,  1, -2],
            [-1,  0,  2],
            [ 4, -1,  5]]
matriceB = [[-1,  4,  2],
            [ 0, -6, -1],
            [ 8,  6, -4]]

multiplicationParScalaire :: Num a => a -> [[a]] -> [[a]]
multiplicationParScalaire k mat = (map . map) (*k) mat

sommeMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
sommeMatrices mat1 mat2 = (zipWith . zipWith) (+) mat1 mat2

produitScalaire :: Num a => [a] -> [a] -> a
produitScalaire xs ys = sum $ zipWith (*) xs ys

produitMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
produitMatrices mat1 mat2 = [[ produitScalaire xs ys | ys <- (transpose mat2)] | xs <- mat1 ]

-- Section 5
-- Voir fichier TicTacToe.hs
