-- Un vecteur 3D générique
data Vecteur a = Vecteur a a a deriving Show

-- Une image
data Image a = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[a]]
} deriving Show

instance  Functor Vecteur  where
    fmap f (Vecteur a b c) = Vecteur (f a) (f b) (f c)
    
instance  Functor Image  where
    fmap f (Image h l p) = Image h l ((map.map) f p)
    
data Liste a = Vide | Cons a (Liste a) deriving Show

instance  Functor Liste  where
    fmap f Vide = Vide
    fmap f (Cons h t) = Cons (f h) (fmap f t)
    
listeEntiers = Cons 1 (Cons (-5) (Cons (-30) (Cons 4 Vide) ) )

retourneListe Vide = [] 
retourneListe (Cons m l) = (m: retourneListe l)

instance Applicative Liste where
    pure x = Cons x $ pure x
    _ <*> Vide = Vide
    Vide <*> _ = Vide
    Cons f fs <*> Cons x xs = Cons (f x) $ fs <*> xs
    lEntiers = Cons 1 (Cons (-5) (Cons 3 Vide ))
    lFonctions = Cons (+ 1) (Cons (*2) (Cons (+2) Vide) )
