# Foncteurs + Entrées / Sorties
## 1 - Foncteurs

### 1.1 Considérez les déclarations suivantes :
```haskell
-- Un vecteur 3D générique
data Vecteur a = Vecteur a a a
-- Une image
data Image a = Image { 
                  hauteur :: Int, 
                  largeur :: Int, 
                  pixels :: [[a]] }
```

En considérant ces définitions, faite des types `Vecteur` et `Image` des instances de
`Functor` et écrire la fonction `fmap` associée. On s'attend au comportement suivant: 
```haskell
Prelude> fmap (+3) (Vecteur 2 5 1)
Vecteur 5 8 4
Prelude> let image = Image 2 3 [[0,1,1],[1,0,1]]
Prelude> image
Image {hauteur = 2, largeur = 3, pixels = [[0,1,1],[1,0,1]]} 
Prelude> fmap (1-) image
Image {hauteur = 2, largeur = 3, pixels = [[1,0,0],[0,1,0]]}
```
### 1.2 Soit le type Liste suivant : 
```haskell
data Liste a = Vide | Cons a (Liste a) deriving Show 
```
Et l’exemple d’une Liste
```haskell
listeEntiers = Cons 1 (Cons (-5) (Cons (-30) (Cons 4 Vide) ) )
```
**Q1:** En considérant cette définition, faite du type `Liste` une instance de `Functor` et écrire la fonction `fmap` associée. On s'attend au comportement suivant :
```haskell
Prelude> fmap (>0) listeEntiers
Cons True (Cons False (Cons False (Cons True Vide))) 
Prelude> fmap (+4) listeEntiers
Cons 5 (Cons (-1) (Cons (-26) (Cons 8 Vide))) 
```
**Q2:** Écrivez une fonction `retourneListe` qui transforme une liste de type `Liste` en une liste de type `[]`.
```haskell
Prelude> retourneListe listeEntiers 
[1,-5,-30,4]
```
Soit le code suivant : 
```haskell
instance Applicative Liste where
    pure x = Cons x $ pure x
    _ <*> Vide = Vide
    Vide <*> _ = Vide
    Cons f fs <*> Cons x xs = Cons (f x) $ fs <*> xs
    lEntiers = Cons 1 (Cons (-5) (Cons 3 Vide ))
    lFonctions = Cons (+ 1) (Cons (*2) (Cons (+2) Vide) )
```
**Q3:** En considérant que vous avez fait du type `Liste` une instance de `Functor` et que vous avez écrit la fonction `fmap` associée, pour chacune des expressions suivantes ́ecrire la  ́eponse que va produire ghci.
* ` [(*2),(+4)] <*> [1,2,3] `
* `pure (*4) <*> lEntiers `
* `(fmap (+) lEntiers ) <*> lEntiers `


## 2. Entrées/Sorties
Complétez le script suivant en y implémentant toutes les fonctions du menu. Un fichier `Labo6_menu.hs` vous est fourni à cet effet. Ce script sert à la gestion de véhicules et on doit pouvoir:
* Visualiser les vehicules 
* Ajouter un vehicule
* Retirer un vehicule
* Charger un fichier
* Sauvegarder dans un fichier

Le dialogue peut ressembler à ceci: 
```haskell
Labo6> menu
*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 1 
* VOITURE 
Modele: Ford
Constructeur: Festiva 
Couleur: gris 
Immatriculation: ASD 987

* CAMION
Modele: GMC 
Constructeur: 12345
Nb. essieux: 3 
Immatriculation: FGH 123

* MOTO
Modele: Honda 
Constructeur: X600 
Immatriculation: ZXC 321
=== Fin de la liste. ===

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 2 
>Nouveau vehicule: 
>Type (V/C/M): x
Type invalide.
>Nouveau vehicule: 
>Type (V/C/M): V
>Modele: Excel
>Constructeur: Hyundai 
>Couleur: gris
>Immatriculation: DFG 654 

* VOITURE
Modele: Excel Constructeur: Hyundai Couleur: gris Immatriculation: DFG 654
=== Fin de la liste. ===

*** MENU ***
1- Visualiser les vehicules 
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 1 
* VOITURE
Modele: Excel 
Constructeur: Hyundai 
Couleur: gris 
Immatriculation: DFG 654

* VOITURE
Modele: Ford 
Constructeur: Festiva 
Couleur: gris 
Immatriculation: ASD 987

* CAMION
Modele: GMC 
Constructeur: 12345
Nb. essieux: 3 
Immatriculation: FGH 123

* MOTO
Modele: Honda 
Constructeur: X600 
Immatriculation: ZXC 321
=== Fin de la liste. ===

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 3 
>Immatriculation du vehicule a retirer: ASD 986 
Ce vehicule n'existe pas.

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 3 
Immatriculation du vehicule a retirer: ASD 987 

* VOITURE
Modele: Ford 
Constructeur: Festiva 
Couleur: gris 
Immatriculation: ASD 987
=== Fin de la liste. ===
Etes-vous sur de vouloir retirer ce vehicule (O/N)? O

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 4
Vehicules chargés avec succès

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier
0- Quitter
Option: 5
Vehicules enregistrés avec succès

*** MENU ***
1- Visualiser les vehicules
2- Ajouter un vehicule
3- Retirer un vehicule
4- Charger un fichier
5- Sauvegarder dans un fichier 
0- Quitter
Option: 0 
Bye Bye!
```