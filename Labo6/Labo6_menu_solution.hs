import Data.List
import System.Environment
import System.IO
import System.Directory
-- Partie B
-- Vous devez creer un fichier vide nomme EnregistrementsDemo6.txt pour le fonctionnement des choix 4 et 5

data Vehicule= 
    Voiture String String String String
    | Camion String String Int String
    | Moto String String String
    deriving (Show, Read)

v1= Voiture "Ford" "Festiva" "gris" "ASD 987"
v2= Camion "GMC" "12345" 3 "FGH 123"
v3= Moto "Honda" "X600" "ZXC 321"

constructeur' (Voiture c _ _ _)= c
constructeur' (Camion c _ _ _)= c
constructeur' (Moto c _ _)= c

immatriculation' (Voiture _ _ _ i)= i
immatriculation' (Camion _ _ _ i)= i
immatriculation' (Moto _ _ i)= i

instance Eq Vehicule where
    (==) x y = immatriculation' x == immatriculation' y

instance Ord Vehicule where
    compare x y
        | immatriculation' x == immatriculation' y = EQ
        | immatriculation' x < immatriculation' y = LT
        | otherwise = GT


menu = menu' [v1, v2, v3]

menu' vs = do
       putStr "\n   *** MENU ***\n"
       putStr "1- Visualiser les vehicules\n"
       putStr "2- Ajouter un vehicule\n"
       putStr "3- Retirer un vehicule\n"
       putStr "4- Charger un fichier\n"
       putStr "5- Sauvegarder dans un fichier\n"
       putStr "0- Quitter\n"
       putStr "   Option: "
       rep <- getLine
       putStr "\n"
       traiterChoix vs rep

traiterChoix vs "1" = do 
                   montrerVehicules vs
                   menu' vs


traiterChoix vs "2" = do
                   v <- ajoutVehicule
                   montrerVehicules [v]
                   menu' (v:vs)
traiterChoix vs "3" = do 
                   vsp <- retirerVehicule vs
                   menu' vsp
				   
traiterChoix vs "4" = do
                   vsp <- lire 
                   menu' vsp

traiterChoix vs "5" = do
                   sauver vs
                   menu' vs
  
traiterChoix _ "0" = do
                   putStr "Bye Bye!\n"

traiterChoix vs c = do
                   putStr "Erreur: \""
                   putStr c
                   putStr "\" n'est pas un choix valide\n"
                   menu' vs
-- Plusieurs autres solutions existent. Par exemple, on peut passer par l'instanciation de la classe show pour chaque type de vehicule etc...
montrerVehicules (v:vs) = do
  montrerUnVehicule v
  montrerVehicules vs
montrerVehicules [] = return ()

montrerUnVehicule (Voiture marque modele couleur imm) = do
    putStrLn "\t* VOITURE"
    putStrLn ("Marque : " ++ marque)
    putStrLn ("Modele : " ++ modele)
    putStrLn ("Couleur : " ++ couleur)
    putStrLn ("Immatriculation : " ++ imm)
    putStrLn ""

montrerUnVehicule (Camion marque modele nbe imm) = do
    putStrLn "\t* CAMION"
    putStrLn ("Marque : " ++ marque)
    putStrLn ("Modele : " ++ modele)
    putStrLn ("Nb. essieux : " ++ (show nbe))
    putStrLn ("Immatriculation : " ++ imm)
    putStrLn ""

montrerUnVehicule (Moto marque modele imm) = do
    putStrLn "\t* MOTO"
    putStrLn ("Marque : " ++ marque)
    putStrLn ("Modele : " ++ modele)
    putStrLn ("Immatriculation : " ++ imm)
    putStrLn ""

ajoutVehicule = do
    putStrLn "\t Nouveau vehicule:"
    putStrLn "\t Type (V/C/M): "
    rep <- getLine
    case (head rep) of
       'V' -> ajoutVoiture
       'C' -> ajoutCamion
       'M' -> ajoutMoto
       _ -> ajoutVehiculeErreur

ajoutVehiculeErreur = do
  putStr "Type invalide \n"
  ajoutVehicule
  
ajoutVoiture = do
  putStr "Marque : "
  m <- getLine
  putStr "Modele : "
  mo <- getLine
  putStr "Couleur : "
  c <- getLine
  putStr "Immatriculation : "
  i <- getLine
  return (Voiture m mo c i)

ajoutCamion = do
  putStr "Marque : "
  m <- getLine
  putStr "Modele : "
  mo <- getLine
  putStr "Nb. essieux : "
  nbe <- getLine
  putStr "Immatriculation : "
  i <- getLine
  return (Camion m mo (read nbe) i)

ajoutMoto = do
  putStr "Marque : "
  m <- getLine
  putStr "Modele : "
  mo <- getLine
  putStr "Immatriculation : "
  i <- getLine
  return (Moto m mo i)

retirerVehicule vs = do
  putStr "Immatriculation du vehicule a retirer : "
  i <- getLine 
  case find ((i ==) . immatriculation') vs of
    Nothing -> do
      putStrLn "Ce vehicule n'existe pas."
      return vs
    Just vp -> do
      montrerVehicules [vp]
      putStrLn "Etes-vous sur de vouloir retirer ce vehicule (O/N) ? "
      rep <- getLine
      if (head rep) == 'O' then
          return (filter (/= vp) vs)
      else
          return vs

sauver vs = do
            fileExists <- doesFileExist "EnregistrementsDemo6.txt"
            if fileExists
              then do putStr "Vehicules enregistres avec succes"
		      writeFile "EnregistrementsDemo6.txt" (show vs)
              else do putStrLn ("Erreur de lecture du fichier ")

lire = do
       fileExists <- doesFileExist "EnregistrementsDemo6.txt"
       if fileExists
              then do contents <- readFile "EnregistrementsDemo6.txt"
	              putStr "Vehicules charges avec succes"
                      return (read contents)
              else do putStr "Fichier non existant"
	              return (read "")