module Labo6 where

data Vehicule= 
    Voiture String String String String
    | Camion String String Int String
    | Moto String String String
    deriving (Show, Read)

v1= Voiture "Ford" "Festiva" "gris" "ASD 987"
v2= Camion "GMC" "12345" 3 "FGH 123"
v3= Moto "Honda" "X600" "ZXC 321"

constructeur' (Voiture c _ _ _)= c
constructeur' (Camion c _ _ _)= c
constructeur' (Moto c _ _)= c

immatriculation' (Voiture _ _ _ i)= i
immatriculation' (Camion _ _ _ i)= i
immatriculation' (Moto _ _ i)= i

instance Eq Vehicule where
    (==) x y = immatriculation' x == immatriculation' y

instance Ord Vehicule where
    compare x y
        | immatriculation' x == immatriculation' y = EQ
        | immatriculation' x < immatriculation' y = LT
        | otherwise = GT


menu = menu' [v1, v2, v3]

menu' vs = do
       putStr "\n   *** MENU ***\n"
       putStr "1- Visualiser les vehicules\n"
       putStr "2- Ajouter un vehicule\n"
       putStr "3- Retirer un vehicule\n"
       putStr "4- Charger un fichier\n"
       putStr "5- Sauvegarder dans un fichier\n"
       putStr "0- Quitter\n"
       putStr "   Option: "
       rep <- getLine
       putStr "\n"
       traiterChoix vs rep

traiterChoix vs "1" = do
                   montrerVehicules vs
                   menu' vs

traiterChoix vs "2" = do
                   v <- ajoutVehicule
                   montrerVehicules [v]
                   menu' (v:vs)

traiterChoix _ "0" = do
                   putStr "Bye Bye!\n"

traiterChoix vs c = do
                   putStr "Erreur: \""
                   putStr c
                   putStr "\" n'est pas un choix valide\n"
                   menu' vs