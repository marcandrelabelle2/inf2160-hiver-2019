# Unification + Écriture de règles + Listes + Modélisation

## 1 - Unification
Pour chacun des buts suivants, dire quel est le résultat renvoyé par l’interpréteur Prolog
```prolog
?- X=2+5.
?- X is 9+2, Y = X*2.
?- X is Y+1, Y is 3.
?- p(1 +X+ 7, Z+ (2 +V)) = p(Y+Z, Y).
?- f(g(X,a),Y)=f(Y,g(b,Z)).
```
## 2 - Règles simples

### 2.1- Définissez le prédicat `pair(X)` qui dit si un nombre est pair.
```prolog
?- pair(3).
false.
?- pair(4). 
true.
?- pair(0). 
true.
?- pair(-8). 
false.
?- pair(X). 
X=0.
```

### 2.2- Définissez le prédicat `somme(N,X)` qui calcul la somme des N premiers entiers. (Où : somme(N,X) est vrai si X est la somme des entiers de 1 à N.)
```prolog
?- somme(0,0). 
true.
?- somme(5,S).
S = 15 ? % normal ! car 1+2+3+4+5=15 
true.
```

### 2.3- Définissez le prédicat `maximum1` qui prend 3 paramètres et détermine le maximum entre deux nombres X et Y (les 2 premiers paramètres) et le retourne dans le 3eme paramètre.
```prolog
?- maximum1(1,2,2). 
true.
?- maximum1(1,2,3). 
false.
?- maximum1(5,2,2). 
false.
?- maximum1(5,2,Z). 
Z=5;
false.
?- maximum1(X,1,3). 
X = 3.
```

### 2.4- Définissez le prédicat `factoriel(X,Y)`, qui calcul le factoriel de X et met le résultat dans Y.
```prolog
?- factoriel(0,Y). 
Y = 1.
?- factoriel(5,Y).
Y = 120.
?- factoriel(-2,Y). 
false.
?- factoriel(5,120). 
true.
?- factoriel(5,5). 
false.
```

### 2.5- Définissez le prédicat `fibo(N,X)` qui est vrai si X est la valeur de la suite de Fibonacci au rang N.
**Rappel** : <a href="https://www.codecogs.com/eqnedit.php?latex=F_0=0,&space;F_1=1,&space;F_n=&space;F_{n-2}$&space;&plus;&space;$F_{n-1}$&space;pour&space;$n&space;\geq&space;2" target="_blank"><img src="https://latex.codecogs.com/svg.latex?F_0=0,&space;F_1=1,&space;F_n=&space;F_{n-2}$&space;&plus;&space;$F_{n-1}$&space;pour&space;$n&space;\geq&space;2" title="F_0=0, F_1=1, F_n= F_{n-2}$ + $F_{n-1}$ pour $n \geq 2" /></a>. 
```prolog
?- fibo(0,0).
true.
?- fibo(1,1). 
true.
?- fibo(2,X). 
X=1.
?- fibo(5,X). 
X=5.
?- fibo(6,6). 
false.
?- fibo(6,X). 
X=8.
```

### 2.6- Définissez le prédicat `suite(X,Y)` calculant le **n_ieme** terme (X) de la suite : 
<a href="https://www.codecogs.com/eqnedit.php?latex=U_0&space;=2,&space;U_n=2&space;U_{n-1}&plus;3" target="_blank"><img src="https://latex.codecogs.com/svg.latex?U_0&space;=2,&space;U_n=2&space;U_{n-1}&plus;3" title="U_0 =2, U_n=2 U_{n-1}+3" /></a>.
```prolog
?- suite(0,X). 
X=2.
?- suite(5,X). 
X = 157 .
?- suite(5,157). 
true.
?- suite(-3,X). 
false.
```

### 2.7- Définissez le prédicat `nombresPairs(X)`, qui génère tous les nombres pairs existants, à la demande de l’utilisateur (après chaque “;”).
```prolog
?- nombrePairs(X). 
X=0;
X=2;
X=4;
X=6; 
X=8; 
X=10; 
X=12; 
X=14 .
```

### 2.8- Redéfinissez le prédicat `max2` qui prend 3 paramètres et détermine le maximum entre deux nombres X et Y (les 2 premiers paramètres) et le retourne dans le 3eme paramètre. 
**Remarque** : Vous devez utiliser cette fois ci le cut `(!)` ou coupe-choix.

### 2.9- Définissez le prédicat `racineCarree(N, R)` qui calcul la racine carré d’un nombre N et retourne le résultat dans le 2eme paramètre R.
**Remarque** : Pour calculer la racine carrée entière d’un nombre, on utilise un générateur de nombres entiers `entier(k)`.
```prolog
?- racineCarree(4,X). 
X = 2.
?- racineCarree(16,4). 
true.
?- racineCarree(16,3). 
false.
```

## 3- Listes
Proposez une implémentation Prolog des prédicats suivants.

### 3.1- Définissez un prédicat `concat(L1, L2, L)`, qui indique si L est la liste obtenue en concaténant L1 et L2. 
```prolog
?- concat([], [], L).
L = [].
?- concat([1,2,3], [4,5], L).
L = [1, 2, 3, 4, 5].
?- concat([1,2],[2,1], L).
L = [1, 2, 2, 1].
?- concat([1,2,3], L, [1,2,3,4,5]).
L = [4, 5].
?- concat([1,2,3], L, [4,5]).
false.
```


### 3.2- Définissez un prédicat `renverse(L1, L2)`, qui indique si L1 est la liste obtenue de L2 en renversant l'ordre dans lequel les éléments sont écrits.
**Indice**: N'hésitez pas à utiliser le prédicat `concat` défini plus haut si vous le pouvez. 
```prolog
?- renverse([], []).
true.
?- renverse([1,2],[2,1]). 
true.
?- renverse([alice], [alice]). 
true.
?- renverse([alpha, beta], [alpha, beta]). 
false.
```

### 3.3- Définissez un prédicat `palindrome(L)`, qui indique si L est une liste palindromique, c'est-à-dire que le premier élément est égal au dernier, le deuxième à l'avant-dernier, etc.
**Indice**: N'hésitez pas à utiliser le prédicat `concat` défini plus haut. On s'attend donc au comportement suivant:
```prolog
?- palindrome([]). 
true.
?- palindrome([alice]). 
true.
?- palindrome([1,2,3]). 
false.
?- palindrome([1,2,1]). 
true.
?- palindrome([alpha, beta, beta, alpha]). 
true.
```


## 4- Modélisation
En géométrie de base, il existe différents types d'objets en 2D. Par exemple,

* Un **cercle**;
* Un **polygone**, qui a un nombre arbitraire de côtés (au moins 3);
* Un **triangle**, qui est un polygone de 3 côtés;
* Un **quadrilatère**, qui est un polygone de 4 côtés;


Parmi les **quadrilatères**, on peut raffiner le type également:
* Un **trapèze**, qui a au moins 1 paire de côtés parallèles;
* Un **parallélogramme**, qui a 2 paires de côtés parallèles;
* Un **rectangle**, qui a 4 angles droits;
* Un **losange**, qui a 4 côtés de même longueur et 2 paires de côtés parallèles;
* Un **carré**, qui a 4 côtés de même longueur et 4 angles droits;

Supposons que les atomes qui nous intéressent sont : cercle, polygone, triangle, quadrilatere, trapeze, parallelogramme, rectangle, losange, carre.

En utilisant des règles bien choisies, proposez un modèle pour ces différents objets qui supporte les requêtes suivantes:
```prolog
est_cercle(X) 
est_polygone(X) 
est_triangle(X) 
est_quadrilatere(X) 
est_trapeze(X) 
est_parallelogramme(X) 
est_rectangle(X) 
est_losange(X)
```

N'oubliez pas qu'un carré est un cas particulier de rectangle et de losange. Par contre, un carré n'est pas un cercle.

Évidemment, il est possible de définir ces fonctions en faisant tous les cas possibles, mais il existe une solution plus intéressante qui permet de faire de l'inférence. Par exemple, on sait que si X est un rectangle, alors c'est forcément un parallélogramme, un trapèze et un quadrilatère.
