pair(0).
pair(X) :- X>0, X2 is X-2, pair(X2).

pair2(X) :- 0 is mod(X,2).

somme(0,0).
somme(X,R) :- X>0, X1 is X-1, somme(X1, R1), R is X+R1.

somme1(X,R) :- R is X*(X+1)/2.

maximal(X,Y,X) :- X>=Y.
maximal(X,Y,Y) :- Y>X.

max2(X,Y,X) :- X>=Y, !.
max2(_,Y,Y).

factoriel(0,1).
factoriel(X,Y) :- X>0, X1 is X-1, factoriel(X1,R1), Y is X*R1.

fibo(0,0).
fibo(1,1).
fibo(N,R) :- N>1, N1 is N-1, N2 is N-2, fibo(N1,R1), fibo(N2,R2), R is R1 + R2.

suite(0,2).
suite(N,R) :- N>0, N1 is N-1, suite(N1,R1), R is 2*R1+3.

nombresPairs(0).
nombresPairs(X) :- nombresPairs(X2), X is X2+2.

entier(0).
entier(X) :- entier(X1), X is X1+1.
racine(X,R) :- entier(N), N*N>X, !, R is N-1.

concat([], L, L).
concat(L, [], L). % Pour rapidité
concat([X|XS], L, [X|YS]) :- concat(XS, L, YS).

renverse([],[]).
renverse([X|XS], RS) :- renverse(XS, YS), concat(YS, [X], RS).

palindrome(L) :- renverse(L,L).

est_cercle(cercle).

est_polygone(polygone).
est_polygone(X) :- est_quadrilatere(X).
est_polygone(X) :- est_triangle(X).

est_triangle(triangle).

est_quadrilatere(quadrilatere).
est_quadrilatere(X) :- est_trapeze(X).

est_trapeze(trapeze).
est_trapeze(X) :- est_parallelogramme(X).

est_parallelogramme(parallelogramme).
est_parallelogramme(X) :- est_rectangle(X).
est_parallelogramme(X) :- est_losange(X).

est_rectangle(rectangle).
est_rectangle(carre).

est_losange(losange).
est_losange(carre).
