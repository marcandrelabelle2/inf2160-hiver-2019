/* ========= */
/* les faits */
/* ========= */

/* les hommes */
homme(andre).
homme(bernard).
homme(babar).
homme(clement).
homme(dudulle).
homme(damien).
homme(baptiste).
homme(cedric).
homme(didier).
homme(dagobert).

/* les femmes */
femme(augustine).
femme(becassine).
femme(brigitte).
femme(chantal).
femme(celestine).
femme(caroline).
femme(charlotte).
femme(daniela).
femme(dominique).

/* les relations de parenté 

% enfant(X,Y) = X enfant de Y ...  */


enfant(bernard,andre).
enfant(bernard,augustine).
enfant(babar,andre).
enfant(babar,augustine).
enfant(brigitte,andre).
enfant(brigitte,augustine).
enfant(clement,bernard).
enfant(clement,becassine).
enfant(celestine,babar).
enfant(caroline,brigitte).
enfant(caroline,baptiste).
enfant(cedric,brigitte).
enfant(cedric,baptiste).
enfant(dudulle,clement).
enfant(dudulle,chantal).
enfant(damien,clement).
enfant(damien,chantal).
enfant(daniela,clement).
enfant(daniela,chantal).
enfant(didier,cedric).
enfant(didier,charlotte).
enfant(dagobert,cedric).
enfant(dagobert,charlotte).
enfant(dominique,cedric).
enfant(dominique,charlotte).

