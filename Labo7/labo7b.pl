aliment(pomme, fruit, sucre, rouge).
aliment(orange, fruit, sucre, orange).
aliment(cerise, fruit, sucre, rouge).
aliment(banane, fruit, sucre, jaune).
aliment(navet, legume, amer, blanc).
aliment(tomate, fruit, sucre, rouge).
aliment(carotte, legume, sucre, orange).
aliment(laitue, legume, amer, vert).
aliment(citron, fruit, acidule, jaune).
aliment(lime, fruit, acidule, vert).
aliment('chou de Bruxelles', legume, amer, vert).
aliment(ketchup, condiment, acidule, rouge).
aliment(relish, condiment, sucre, vert).