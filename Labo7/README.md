# Initiation à Prolog
L’environnement prolog choisit est `swi-prolog` (disponible en libre téléchargement sur le site officiel [prolog](http://www.swi-prolog.org/)) et disponible également sur les ordinateurs de l’UQAM.

## La base de faits
Soit les faits suivants, contenus dans le fichier `labo7a.pl`.
Les prédicats `aliment`, `gout` et `couleur`, définissent la catégorie, le goût et la couleur d'un aliment. Un tel fichier s’appelle  **base de faits** ou **base de connaissances** (BC), parce qu’il regroupe des faits que l’on déclare être vrais.

## Application
**Utilisateurs Windows** : Ouvrez swi Prolog. Dans le menu, choisissez **File**, puis **Consult** (Load pour sicstus-prolog) pour ouvrir votre script prolog (**fichier.pl**).

**Ligne de commande (Linux)**: Ouvrir une invite de commandes et saisissez `swipl fichier.pl`, ou `prolog fichier.pl`. Remplacez `fichier.pl`, par le nom de votre fichier
script. Vous pouvez également, une fois prolog ouvert (avec la commande **prolog** ou
**swipl** faire : `[fichier.pl]`.
Pour mettre à jour la BC lorsqu’elle a été modifiée :
* Windows : File -> Reload modified files
* Linux : **make.**

## Requêtes simples : Exemple
Demandez à Prolog si une pomme est un fruit. Demandez ensuite à Prolog quels aliments sont des fruits. Le dialogue peut ressembler à ceci:
```prolog
| ?- aliment(pomme, fruit).
true.
| ?- aliment(A, fruit).
A = pomme.
| ?- aliment(A, fruit).
A = pomme ;
A = orange ;
A = cerise ;
A = banane ;
```

## Requêtes simples: Questions
Écrivez les requêtes ci-dessous:
* Comment savoir si un navet est sucré ?
* Quel est la liste des légumes ?
* Quel est la couleur d'une pomme ?
* Quels sont les fruits sucrés?
* Quels aliments sont de couleur orange et de goût sucré ?
* Quels légumes sont verts ?

**Remarque** : Notez que vous pouvez à tout moment modifier la **BC** pour ajouter des faits et des règles et demander à Prolog de la Reconsulter (menu File -> Reconsult). Notez
aussi que vous pouvez naviguer dans la fenêtre de l'interpréteur, modifier d'anciennes questions et les (re)poser à Prolog etc...

## Règles simples: Questions
Soit la base de connaissances contenue dans le fichier `labo7b.pl`.
Les faits inclus dans cette BC décrivent des aliments avec leur `nom`, leur `type`, leur `goût` et leur `couleur`.

##### Définissez le prédicat `meme_gout(Aliment1, Aliment2)` qui donne les paires d'aliments de même goût. Par exemple:
```prolog
% Est-ce qu'une pomme et un citron ont le même gout?
| ?- meme_gout(pomme, citron).
false.
% Est-ce qu'une pomme et une orange ont de même goût?
| ?- meme_gout(pomme, orange).
true.
% Quels aliments ont le même gout qu'un navet?
| ?- meme_gout(A, navet).
A = navet ; (...oui, un navet a le même goût que lui-même)
A = laitue ;
A = 'chou de Bruxelles'? ;
false.
% Quels paires d'aliments ont le même goût?
| ?- meme_goût(A1, A2).
A1 = pomme,
A2 = pomme ;
A1 = pomme,
A2 = orange ;
A1 = pomme,
A2 = cerise ;
Etc..
```

##### Définissez les prédicats `meme_type` et `meme_couleur` de la même manière.

##### Définissez le prédicat `similaires(Aliment1, Aliment2, Similarité)` qui donne les paires d'aliments similaires (même type, goût ou couleur) avec leur(s) similarité(s). Par exemple:
```prolog
% Une pomme et un navet sont-ils similaires?
| ?- similaires(pomme, navet, _).
false.
% Quelle est la similarité (S) entre une pomme et un citron?
| ?- similaires(pomme, citron, S).
S = fruit ;
false.
% ... et entre un chou de Bruxelles et de la laitue?
| ?- similaires(laitue, 'chou de Bruxelles', S).
S = legume ;
S = amer ;
S = vert ;
false.
% Quels aliments sont similaires à une pomme et comment?
| ?- similaires(pomme, A, S).
A = pomme,
S = fruit ;
A = orange,
S = fruit ;
A = cerise,
S = fruit ;
etc.
```


##### Définissez le prédicat `aliment_non_fruit(Aliment)` qui est vrai si le paramètre est un aliment qui n'est pas un fruit. Ce prédicat peut aussi donner tous les aliments qui ne sont pas des fruits. Par exemple:
```prolog
% Est-ce qu'un navet est un "aliment non-fruit"?
| ?- aliment_non_fruit(navet).
true.
% ...et une pomme?
| ?- aliment_non_fruit(pomme).
false.
% ...et un avion?
| ?- aliment_non_fruit(avion).
false.
% Quels sont tous les "aliments non-fruits"?
| ?- aliment_non_fruit(A).
A = navet ;
A = carotte ;
A = laitue ;
A = 'chou de Bruxelles' ;
A = ketchup ;
A = relish ;
false.
```


##### Définissez le prédicat `non_fruit_de_gout(Aliment, Goût)` qui est vrai si l'aliment n'est pas un fruit et qu'il a le goût `Goût`. Ce prédicat peut aussi donner tous les non-fruits d'un certain goût. Par exemple:
```prolog
| ?- non_fruit_de_gout(A, sucre).
A = carotte ;
A = relish ;
false.
| ?- non_fruit_de_gout(A, G).
A = navet,
G = amer ;
A = carotte,
G = sucre ;
A = laitue,
G = amer ;
A = 'chou de Bruxelles',
G = amer ;
A = ketchup,
G = acidule ;
A = relish,
G = sucre.
```

##### Arbre généalogique
Soit la base de connaissances contenue dans le fichier `labo7c.pl`. Définissez les règles suivantes :
* `parent (X,Y)` : Qui vérifie si X est bien le parent de Y.
* `grand_parent(X,Y), petit_enfant(X,Y), grand_père(X,Y), grand_mère(X,Y), petit_fils(X,Y), petite_fille(X,Y)`: `grand_parent(X,Y)`, qui vérifie si X est un grand parent de Y.
* `frere_ou_soeur(X,Y), frère(X,Y), soeur(X,Y)` : Les frères et soeurs doivent avoir le même père et la même mère.
* `oncle_ou_tante(X,Y), oncle(X,Y), tante(X,Y)`

**Notes générales :**
```prolog
/* Ceci est un commentaire sur
plusieurs lignes */
% Un commentaire sur une ligne
```
Commande pour sortir de l’invite prolog : "**halt.**". (N’oubliez pas le point à la fin)
