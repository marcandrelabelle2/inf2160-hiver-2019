aliment(pomme,fruit).
aliment(orange,fruit).
aliment(navet,legume).
aliment(carotte,legume).
aliment(cerise,fruit).
aliment(banane,fruit).

gout(orange,sucre).
gout(navet,amer).
gout(pomme,sucre).
gout(carotte,sucre).

couleur(pomme,rouge).
couleur(orange,orange).
couleur(carotte,orange).

% requete (a) Comment savoir si un navet est sucré = gout(navet,sucre).
% requete (b) Quel est la liste des légumes ? = aliment(A,legume).
% requete (c) Quel est la couleur  d'une pomme ? = couleur(pomme,C).
% requete (d) Quels sont les fruits sucrés? = aliment(A,fruit),gout(A,sucre).
% requete (e) Quels aliments sont de couleur orange et de goût sucré ? = aliment(A,_),couleur(A,orange),gout(A,sucre).
% requete (f) Quels légumes sont verts ? = aliment(A,legume),couleur(A,vert).
