## 1 - Sous-ensembles

Écrivez un prédicat en Prolog qui permet d'énumérer tous les "sous-ensembles"
d'une liste triée. Plus précisément, on s'attend au comportement suivant:
```prolog
?- sous_ensembles([2,4,5], L]).
L = [[], [2], [2, 4], [2, 4, 5], [2, 5], [4], [4, 5], [5]].
```

## 2 - Énumération de chemins

### 2.1 - Être dans un rectangle

Implémentez un prédicat `dans_rectangle(X, Y, W, H)` qui indique si le point
`(X,Y)` se trouve dans le rectangle $`(1,W) \times (1,H)`$. On s'attend donc à
ce que
```prolog
dans_rectangle(1, 5, 4, 7)
```
retourne vrai. Le prédicat prédifini ``between/3`` vous sera utile.

### 2.2 - Rectangle complet

En utilisant le prédicat défini à la sous-question précédente, proposez un
prédicat `rectangle(W, H, P)` qui est vérifié si `P` est une liste qui contient
toutes les positions qui se trouvent dans le rectangle $`(1,W) \times (1,H)`$.

Le prédicat `findall/3` pourrait vous être utile. Il est également intéressant
de comparer le résultat obtenu en utilisant les prédicats `bagof/3` et
`setof/3`.

### 2.3 - Chemins

**Note**: Exercice demandant un peu de concentration :-)

Implémentez un prédicat `chemins(X1, Y1, X2, Y2, W, H, C)` qui énumère tous les
chemins du point `p(X1, Y1)` vers le point `p(X2, Y2)` passant seulement par
des points du rectangle de largeur `W` et de hauteur `H`, et qui stocke le
résultat dans `C`.

On représente un chemin par une liste de points qui sont adjacents deux à deux.
Par exemple, un chemin du point `p(2,4)` au point `p(3,0)` pourrait être donné
par la liste
```prolog
[p(2,4), p(2,3), p(3,3), p(3,2), p(3,1), p(3,0)]
```

Notez que, en principe, le nombre de chemins possible est infini. Par
conséquent, nous allons ajouter la contrainte qu'il est interdit de passer plus
d'une fois par le même point.

**Indice 1**: Il peut être utile de définir un prédicat
```prolog
adjacent(X1, Y1, X2, Y2)
```
qui indique si les points `(X1, Y1)` et `(X2, Y2)` sont adjacents.

**Indice 2**: Utilisez un prédicat auxiliaire
```prolog
chemin(X1, Y1, X2, Y2, W, H, C, I)
```
qui calcule un chemin `C` de `(X1, Y1)` vers `(X2, Y2)` dans un rectangle `W`
par `H` et qui ne peut pas passer par les points présents dans la liste `I`
(bref, `I` signifie *interdit* dans ce contexte). Vous pourrez ensuite plus
facilement définir le prédicat `chemins`.

## 3 - Théorie des graphes

### 3.1 - Modélisation d'un graphe

Soit le graphe suivant à la figure suivante.

![](graph1.png)

Modélisez le graphe en définissant un prédicat ``arc(From, To)``
pour modéliser toutes les arêtes orientées (arcs).
Note: Commentez pour l'instant l'arc allant de `a` à `e`.

### 3.2 - Existence d'un chemin entre deux noeuds

Écrivez un prédicat ``path(Alpha, Omega)`` qui retourne vrai s'il est possible de
se rendre du noeud Alpha jusqu'au noeud Omega en parcourant les arcs du graphe.

```prolog 
?- path(f, a).
true.
?- path(e, a).
true.
?- path(e, f).
false.
?- path(w, e).
false.
```

Une fois que les tests précédents fonctionnent, décommentez l'arc que vous avez
commenté à la section précédente. Est-ce que tous les tests fonctionnent
toujours? Pourquoi?
Essayez de corriger le problème.

Indice: Mémorisez les noeuds qui ont déjà été visités.
